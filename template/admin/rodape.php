<!-- Jquery -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/jquery-3.3.1.min.js"></script>

<!-- vendors para o crop funcionar -->
<script src="<?= PORTAL_URL; ?>assets/js/vendors.min.js"></script>

<!-- Livequery -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/livequery.js"></script>

<!-- JS UTIL -->
<script src="<?= PORTAL_URL ?>operations/utils.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>conf/config.js" type="text/javascript"></script>

<!-- Script -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/script.min.js"></script>

<!-- Máscaras dos Inputs! -->
<script src="<?= PORTAL_URL; ?>assets/plugins/input-mask/input-mask.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?= PORTAL_URL; ?>assets/plugins/sweet-alert2/sweetalert2.min.js"></script>

<!-- Select2 -->
<script src="<?= PORTAL_URL; ?>assets/plugins/select2/js/select2.min.js"></script>

<!-- Máscara de Dinheiro -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/jquery.price_format.1.3.js"></script>