<?php
include_once('conf/config.php');
include_once('conf/Url.php');
include_once('operations/funcoes.php');
$db = Conexao::getInstance();


vf_usuario_login();
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>E-COMMERCE</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">

        <!-- Icons -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= PORTAL_URL; ?>assets/img/9072logotelles.ico">

        <meta name="application-name" content="&nbsp;"/>
        <meta name="theme-color" content="#000000">

        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/admin/lite-purple.min.css">
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/admin/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/admin/datatables.min.css">

        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/admin/style.css">

        <!-- Sweet Alert -->
        <link href="<?= PORTAL_URL; ?>assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
        <!-- App css -->
        <link href="<?= PORTAL_URL; ?>assets/css/cores.css" rel="stylesheet" type="text/css">
        <!-- Select 2 -->
        <link href="<?= PORTAL_URL; ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>