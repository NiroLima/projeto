<div class="main-header">
    <div class="logo">
        <a href="<?= PORTAL_URL; ?>admin/painel"><img src="<?= PORTAL_URL; ?>assets/img/e-commerce.png" alt=""></a>
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div style="margin: auto"></div>
</div>

<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">

            <li class="nav-item" data-item="produtos">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Box-Full"></i>
                    <span class="nav-text">PRODUTOS</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item" data-item="fornecedores">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Truck"></i>
                    <span class="nav-text">FORNECEDORES</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item" data-item="dashboard">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Business-Mens"></i>
                    <span class="nav-text">USUÁRIOS</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item">
                <a class="nav-item-hold sair" href="<?= PORTAL_URL . "admin/logout" ?>">
                    <i class="nav-icon i-Power-2"></i>
                    <span class="nav-text">SAIR</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">

        <ul class="childNav" data-parent="produtos">
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/produtos/cadastro">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Nova</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/produtos/lista">
                    <i class="nav-icon i-Professor"></i>
                    <span class="item-name">Lista</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="fornecedores">
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/fornecedores/cadastro">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Novo</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/fornecedores/lista">
                    <i class="nav-icon i-Professor"></i>
                    <span class="item-name">Lista</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="dashboard">
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/usuarios/cadastro">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Novo</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?= PORTAL_URL; ?>admin/view/usuarios/lista">
                    <i class="nav-icon i-Professor"></i>
                    <span class="item-name">Lista</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>