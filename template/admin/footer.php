<div class="flex-grow-1"></div>
<div class="app-footer">
    <div class="footer-bottom d-flex flex-column flex-sm-row align-items-center">
        <span class="flex-grow-1"></span>
        <div class="d-flex align-items-center">
            <div>
                <p class="m-0">&copy; 2021 E-commerce. Todos os Direitos Reservados</p>
            </div>
        </div>
    </div>
</div>