<?php
@session_start();
include_once('conf/config.php');
include_once('conf/Url.php');
require($_SERVER['DOCUMENT_ROOT'] . '/projeto/classes/carrinho.php');

$cart = new Cart([
    // limitar quantidade items no carrinho
    'cartMaxItem' => 0,
    // setar o máximo de quantidade de items no carrinho
    'itemMaxQuantity' => 99,
    'useCookie' => true,
        ]);

$db = Conexao::getInstance();
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="application-name" content="&nbsp;"/>
        <meta name="theme-color" content="#000000">
        <title>E-COMMERCE</title>

        <link rel="shortcut icon" type="image/x-icon" href="<?= PORTAL_URL; ?>assets/img/9072logotelles.ico">

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,600,500,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Belleza' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>

        <link href="<?= PORTAL_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/fontawesome/4.2.0/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/geral.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/global.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/styleecba.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/media.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/wishlis.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">

    </head>

    <body class="templateIndex">
        <!-- Header -->
        <header id="top" class="fadeInDown clearfix">
            <!--top-->
            <div class="container hidden-xs">
                <div class="top row">

                    <div class="col-md-6 phone-shopping">
                        <span>TELEFONE DE CONTATO (99) 9999-9999</span>
                    </div>

                    <div id="div_modal_car" class="col-md-18">
                        <ul class="text-right">
                            <li id="widget-social">
                                <ul id="icone_carrinho" class="list-inline">
                                    <li><a style="cursor: pointer" class="btooltip swing" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-shopping-cart">&nbsp;<?= $cart->getTotalItem(); ?></i></a></li>
                                </ul>
                                <div id="car_search" class="search-form dropdown-menu carrinho-topo">
                                    <div class="text-center">

                                        <h3 style="color: black">Meu Carrinho</h3>

                                        <hr size="1" style="width: 100%"/>

                                        <?php
                                        $qtd = 0;
                                        $busca = array();

                                        $allItems = $cart->getItems();

                                        foreach ($allItems as $items) {
                                            foreach ($items as $item) {
                                                $qtd++;
                                                array_push($busca, $item['id']);
                                            }
                                        }

                                        if ($qtd > 0) {
                                            $result = $db->prepare("SELECT p.id, p.descricao, p.valor, p.nome, p.foto  
                                                      FROM produtos p 
                                                      WHERE p.id IN (" . implode(",", $busca) . ") 
                                                      ORDER BY p.nome");
                                            $result->execute();
                                        }

                                        $total_itens = 0;
                                        $valor_total = 0;

                                        while ($produto = $result->fetch(PDO::FETCH_ASSOC)) {

                                            $quantidade = $cart->lookItem($produto['id'], "quantity");

                                            $total_itens += $quantidade;
                                            $valor_total += $quantidade * $produto['valor'];
                                            ?>

                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-5"><img src="<?= PORTAL_URL . "" . ($produto['foto'] != "" ? $produto['foto'] : "assets/img_produtos/sem_imagem.png") ?>" style="width: 100%;"/></div>
                                                <div class="col-md-9 produto-12px">
                                                    <?= $produto['nome'] ?>
                                                    <p class="produto-quantidade">Quantidade: <?= $quantidade; ?>x</p>
                                                </div>
                                                <div class="col-md-7 produto-12px"><b>R$ <?= fdec($produto['valor']); ?></b></div>
                                                <div class="col-md-2"></div>
                                            </div>
                                            <hr size="1" style="width: 100%"/>
                                            <?php
                                        }
                                        ?>
                                        <div class="ver-meu-carrinho">
                                            <div class="meu-carrinho-total">Total: <b>R$ <?= fdec($valor_total); ?></b></div>
                                            <br/>  <br/>
                                            <a href="<?= PORTAL_URL; ?>carrinho"><button class="btn btn-primary" style="width: 60%">Ver meu carrinho</button></a>
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--End top-->

            <div class="line"></div>

            <!-- Navigation -->
            <div class="container">
                <div class="top-navigation">
                    <ul class="list-inline">
                        <li class="navigation">
                            <nav class="navbar" role="navigation">
                                <div class="clearfix">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle main navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div class="is-mobile visible-xs">
                                        <ul class="list-inline">
                                            <li class="is-mobile-menu">
                                                <div class="btn-navbar" data-toggle="collapse" data-target=".navbar-collapse">
                                                    <span class="icon-bar-group">
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                    </span>
                                                </div>
                                            </li>

                                            <li class="is-mobile-login">
                                                <div class="btn-group">
                                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <ul class="customer dropdown-menu">

                                                        <li class="logout">
                                                            <a href="<?= PORTAL_URL; ?>admin/login">Admin</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </li>

                                            <li class="is-mobile-cart">
                                                <a href="<?= PORTAL_URL; ?>carrinho"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="collapse navbar-collapse"> 
                                        <ul class="nav navbar-nav hoverMenuWrapper">
                                            <li class="nav-item active">
                                                <a style="cursor: pointer" onclick="deslizar(0)">
                                                    <span>Início</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a style="cursor: pointer" onclick="deslizar(1)">
                                                    <span>Produtos</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="<?= PORTAL_URL; ?>admin/login">
                                                    <span>Admin</span>
                                                </a>
                                            </li>
                                        </ul>       
                                    </div>
                                </div>
                            </nav>
                        </li>

                        <li class="top-search hidden-xs">
                            <div class="header-search">
                                <a href="#">
                                    <span data-toggle="dropdown">
                                        <i class="fa fa-search"></i>
                                        <i class="sub-dropdown1"></i>
                                        <i class="sub-dropdown"></i>
                                    </span>
                                </a>
                                <form id="header-search" class="search-form dropdown-menu" action="<?= PORTAL_URL; ?>detalhes" method="POST">
                                    <input type="hidden" name="type" value="product" />
                                    <input type="text"  id="resultado" name="resultado" value="" accesskey="4" autocomplete="off" placeholder="Pesquisar produto..." />
                                    <button type="submit" class="btn">Buscar</button>
                                </form>
                            </div>
                        </li>

                        <li class="mobile-search visible-xs">
                            <form id="mobile-search" class="search-form" action="<?= PORTAL_URL; ?>detalhes" method="POST">
                                <input type="hidden" name="type" value="product" />
                                <input type="text" class="" id="resultado" name="resultado" value="" accesskey="4" autocomplete="off" placeholder="Pesquisar produto..." />
                                <button type="submit" class="search-submit" title="buscar"><i class="fa fa-search"></i></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!--End Navigation-->
        </header>
