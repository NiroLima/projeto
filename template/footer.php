<footer id="footer">      
    <div id="footer-content">
        <div class="footer-content footer-content-bottom clearfix">
            <div class="container">
                <div class="copyright col-md-12">&copy; 2021 <a href="#">E-commerce</a>. All Rights Reserved.</div> 
            </div>
        </div>
    </div>
</footer>
</body>
</html>

<script src="<?= PORTAL_URL ?>assets/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/jquery.imagesloaded.min.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/jquery.camera.min.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/application6e5b.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/jquery.owl.carousel.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/jquery.bxsliderb991.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/jquery.currencies.minca7e.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>assets/js/script667c.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>operations/utils.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>conf/config.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL; ?>assets/plugins/sweet-alert2/sweetalert2.min.js"></script>