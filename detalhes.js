//------------------------------------------------------------------------------
$('body').mouseover(function () {
    $("div#car_search").slideUp("slow");
});
//------------------------------------------------------------------------------
$('div#div_modal_car').mouseover(function () {
    event.stopPropagation();
});
//------------------------------------------------------------------------------
$("ul#icone_carrinho").mouseover(function () {
    $("div#car_search").slideDown("slow");
    event.stopPropagation();
});
//------------------------------------------------------------------------------
function comprar(codigo) {
    window.onbeforeunload = null;
    projetouniversal.util.getjson({
        url: PORTAL_URL + "salvar_carrinho",
        type: "POST",
        data: {id: codigo},
        enctype: 'multipart/form-data',
        success: onSuccessSend,
        error: onError
    });
    return false;
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {
    if (obj.msg == 'success') {
        postToURL(PORTAL_URL + 'carrinho');
    } else if (obj.msg == 'error') {
        aert('Erro: ' + obj.retorno);
    }
    return false;
}
//------------------------------------------------------------------------------
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    aert('Erro: ' + args.retorno);
}

//------------------------------------------------------------------------------
