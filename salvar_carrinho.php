<?php

require($_SERVER['DOCUMENT_ROOT'] . '/projeto/classes/carrinho.php');

$db = Conexao::getInstance();

$id = @$_POST['id'];

$cart = new Cart([
    // limitar quantidade items no carrinho
    'cartMaxItem' => 0,
    // setar o máximo de quantidade de items no carrinho
    'itemMaxQuantity' => 99,
    'useCookie' => true,
        ]);

$cart->add($id);

//MENSAGEM DE SUCESSO
$msg['id'] = $id;
$msg['msg'] = 'success';
$msg['retorno'] = 'Produto adicionado com sucesso!';
echo json_encode($msg);
exit();
?>
    
