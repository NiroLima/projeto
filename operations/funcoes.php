<?php
//------------------------------------------------------------------------------
function preparar_array_sql($array) {
    $resultado = "";
    $novo = $array;
    rsort($novo);
    $novo = array_unique($novo);
    foreach ($novo AS $obj => $val) {
        $resultado .= "'$val',";
    }

    return substr($resultado, 0, -1);
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA VERIFICAR A CONEXÃO EXTERNA DE URL
function endereco_existe($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $content = curl_exec($ch);
    $info = curl_getinfo($ch);

    if ($info['http_code'] == 200) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
function formato_arquivo($arquivo) {
    $tipo = ctexto(end(explode(".", $arquivo)), 'min');

    if ($tipo == 'pdf') {
        return 'ic-pdf';
    } else if ($tipo == 'excel' || $tipo == 'xls') {
        return 'ic-xls';
    } else if ($tipo == 'doc' || $tipo == 'docx') {
        return 'ic-docx';
    } else if ($tipo == 'pptx') {
        return 'ic-ppt';
    } else if ($tipo == 'jpg' || $tipo == 'png' || $tipo == 'jpg' || $tipo == 'jpeg' || $tipo == 'gif') {
        return 'ic-jpg';
    } else if ($tipo == 'mp3' || $tipo == 'wav') {
        return 'ic-mp3';
    } else if ($tipo == 'avi' || $tipo == 'mp4' || $tipo == 'mkv') {
        return 'ic-avi';
    } else if ($tipo == 'zip' || $tipo == 'rar') {
        return 'ic-zip';
    } else {
        return 'ic-outros';
    }
}

//------------------------------------------------------------------------------
function vf_tipo_pdf($arquivo) {
    $erro = true;

    $formatoArquivo = strrchr($arquivo, ".");

    if ($formatoArquivo == '.pdf' || $formatoArquivo == '.PDF' || $formatoArquivo == 'pdf' || $formatoArquivo == 'PDF') {
        $erro = false;
    }

    return $erro;
}

//------------------------------------------------------------------------------
function resume($var, $limite) {
    // Se o texto for maior que o limite, ele corta o texto e adiciona 3 pontinhos.
    if (strlen($var) > $limite) {
        $var = substr($var, 0, $limite);
        $var = trim($var) . "...";
    }
    return $var;
}

//------------------------------------------------------------------------------
function info_usuario($id_usuario) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT u.nome, o.sigla 
            FROM seg_usuario AS u 
            LEFT JOIN bsc_unidade_organizacional AS o ON o.id = u.unidade_organizacional_id
            WHERE u.id = ?");
    $rs->bindValue(1, $id_usuario);
    $rs->execute();
    $usuario = $rs->fetch(PDO::FETCH_ASSOC);

    return "O usuário <span class='text-info'>" . $usuario['nome'] . " (" . $usuario['sigla'] . ")</span>";
}

//------------------------------------------------------------------------------
function vf_usuario_login() {
    if (isset($_SESSION['id'])) {
     
    } else if (Url::getURL(0) != 'login') {
        echo "<script>window.location = '" . PORTAL_URL . "admin/logout';</script>";
        exit();
    }
}

//------------------------------------------------------------------------------
// FUNÇÃO QUE RECEBE UM ARRAY E ARRUMA TODOS OS DADOS PARA SEREM PEGOS
function retorna_campos($post) {
    $fields = explode("&", $post);
    foreach ($fields as $field) {
        $field_key_value = explode("=", $field);
        $key = ($field_key_value[0]);
        $value = ($field_key_value[1]);
        if ($value != '')
            $data[$key] = (urldecode($value));
    }
    return $data;
}

//------------------------------------------------------------------------------
// MÉTOD PARA VALIDAR O CPF
function valida_cpf($cpfx) {
    $cpf = "";
    $guard = "";

    for ($i = 0; ($i < 14); $i++) {
        if ($cpfx[$i] != '.' && $cpfx[$i] != '-') {
            $cpf += $cpfx[$i];
            $guard = "$guard$cpfx[$i]";
        }
    }

    $cpf = $guard;

    // Verifica se o cpf possui números
    if (!is_numeric($cpf)) {
        $status = false;
    } else {
        // VERIFICA
        if (($cpf == '11111111111') || ($cpf == '22222222222') || ($cpf == '33333333333') || ($cpf == '44444444444') || ($cpf == '55555555555') || ($cpf == '66666666666') || ($cpf == '77777777777') || ($cpf == '88888888888') || ($cpf == '99999999999') || ($cpf == '00000000000')) {
            $status = false;
        } else {
            // PEGA O DIGITO VERIFIACADOR
            $dv_informado = substr($cpf, 9, 2);

            for ($i = 0; $i <= 8; $i++) {
                $digito[$i] = substr($cpf, $i, 1);
            }

            // CALCULA O VALOR DO 10º DIGITO DE VERIFICAÇÂO
            $posicao = 10;
            $soma = 0;

            for ($i = 0; $i <= 8; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[9] = $soma % 11;

            if ($digito[9] < 2) {
                $digito[9] = 0;
            } else {
                $digito[9] = 11 - $digito[9];
            }

            // CALCULA O VALOR DO 11º DIGITO DE VERIFICAÇÃO
            $posicao = 11;
            $soma = 0;

            for ($i = 0; $i <= 9; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[10] = $soma % 11;

            if ($digito[10] < 2) {
                $digito[10] = 0;
            } else {
                $digito[10] = 11 - $digito[10];
            }

            // VERIFICA SE O DV CALCULADO É IGUAL AO INFORMADO
            $dv = $digito[9] * 10 + $digito[10];
            if ($dv != $dv_informado) {
                $status = false;
            } else
                $status = true;
        } // FECHA ELSE
    } // FECHA ELSE(is_numeric)
    return $status;
}

//------------------------------------------------------------------------------
// MÉTODO PARA REALIZAR UMA PESQUISA DE COMPARAÇÃO NO BANCO DE DADOS
function pesquisar($retorno, $tabela, $campo, $cond, $variavel, $add) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT $retorno FROM $tabela WHERE $campo $cond ? $add");
    $rs->bindValue(1, $variavel);
    $rs->execute();
    $dados = $rs->fetch(PDO::FETCH_ASSOC);

    return $dados[$retorno];
}

//------------------------------------------------------------------------------
// MÉTODO PARA REALIZAR UMA PESQUISA DE COMPARAÇÃO NO BANCO DE DADOS
function pesquisar2($retorno, $tabela, $campo, $cond, $variavel, $campo2, $cond2, $variavel2, $add) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT $retorno FROM $tabela WHERE $campo $cond ? AND $campo2 $cond2 ? $add");
    $rs->bindValue(1, $variavel);
    $rs->bindValue(2, $variavel2);
    $rs->execute();
    $dados = $rs->fetch(PDO::FETCH_ASSOC);

    return $dados[$retorno];
}

//------------------------------------------------------------------------------
// Método para verificar se o email informado é do ac.gov.br
function VfEmailAc($email) {
    $emailDigitado = ""; // vai guardar tudo que vem depois do @ para depois comparar
    $vf = false;
    $emailvf = false;

    for ($k = 0; ($k < strlen($email)); $k++) {
        if ($email[$k] == '@' || $vf == true) {
            $emailDigitado = "$emailDigitado$email[$k]";
            $vf = true;
        }
    }
    if ($emailDigitado == "@ac.gov.br") {
        $emailvf = true;
        return true;
    } else if ($emailvf == false) {
        return false;
    }
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O TIPO DE ACESSO
function tipo_acesso($codigo) {
    if ($codigo == 1)
        return "A";
    else
        return "B";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status($codigo) {
    if ($codigo == 1)
        return "Ativo";
    else
        return "Inativo";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status_visualizacao($codigo) {
    if ($codigo == 1)
        return "Visualizado";
    else
        return "Não Visualizado";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR A SITUAÇÃO
function situacao($codigo) {
    if ($codigo == 0)
        return "Cadastrando";
    else if ($codigo == 1)
        return "Análise";
    else if ($codigo == 2)
        return "Confirmado";
    else
        return "Cadastrando";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR A SITUAÇÃO
function situacao_reuniao($codigo) {
    if ($codigo == 0)
        return "Aguardando Confirmação";
    else if ($codigo == 1)
        return "Confirmado";
    else if ($codigo == 2)
        return "Finalizado";
    else if ($codigo == 3)
        return "Indisponível";
    else if ($codigo == 4)
        return "Cancelado";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status_inverso($codigo) {
    if (strtoupper($codigo) == "ATIVO")
        return 1;
    else if (strtoupper($codigo) == "INATIVO")
        return 0;
    else
        return 2;
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O ESTADO DE UM MUNICÍPIO
function estado_municipio($municipio) {
    if (is_numeric($municipio)) {
        $con = Conexao::getInstance();

        $rs = $con->prepare("SELECT estado_id FROM bsc_cidade WHERE id = ?");
        $rs->bindValue(1, $municipio);
        $rs->execute();
        $dados = $rs->fetch(PDO::FETCH_ASSOC);

        return $dados['estado_id'];
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
// CALCULA A DIFERENÇA ENTRE MESES ENTRE DUAS DATAS
function diff_data_meses($inicio, $fim) {
    if ($inicio != "00/00/0000 00:00:00" && $fim != "00/00/0000 00:00:00" && $inicio != "0000-00-00 00:00:00" && $fim != "0000-00-00 00:00:00") {
        // CONVERTE AS DATAS PARA O FORMATO AMERICANO
        $inicio = explode('/', $inicio);
        $inicio = "{$inicio[2]}-{$inicio[1]}-{$inicio[0]}";

        $fim = explode('/', $fim);
        $fim = "{$fim[2]}-{$fim[1]}-{$fim[0]}";

        // AGORA CONVERTEMOS A DATA PARA UM INTEIRO
        // QUE REPRESENTA A DATA E É PASSÍVEL DE OPERAÇÕES SIMPLES
        // COMO SUBITRAÇÃO E ADIÇÃO
        $inicio = strtotime($inicio);
        $fim = strtotime($fim);

        // CALCULA A DIFERENÇA ENTRE AS DATAS
        $intervalo = $fim - $inicio;

        $meses = floor(($intervalo / (30 * 60 * 60 * 24)));

        if ($meses > 1) {
            return "$meses meses";
        } else if ($meses == 1) {
            return "$meses mês";
        } else if ($meses == 0) {
            return "Esse mês";
        } else if ($meses < 0) {
            if ($meses < - 1) {
                return "Atrasado " . abs($meses) . " meses";
            } else {
                return "Atrasado " . abs($meses) . " mês";
            }
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
// CALCULA A DIFERENÇA ENTRE DIAS ENTRE DUAS DATAS
function diff_data_dias($data_inicial, $data_final) {
    if ($data_inicial != "00/00/0000 00:00:00" && $data_final != "00/00/0000 00:00:00" && $data_inicial != "0000-00-00 00:00:00" && $data_final != "0000-00-00 00:00:00") {

        $diferenca = strtotime($data_final) - strtotime($data_inicial);

        $dias = floor($diferenca / (60 * 60 * 24));

        if ($dias > 1) {
            return "$dias Dias";
        } else if ($dias == 1) {
            return "$dias Dia";
        } else if ($dias == 0) {
            return "Hoje";
        } else if ($dias < 0) {
            if ($dias < - 1) {
                return "" . abs($dias) . " Dias";
            } else {
                return "" . abs($dias) . " Dia";
            }
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
function diff_data_dias2($data_inicial, $data_final) {
    if ($data_inicial != "00/00/0000 00:00:00" && $data_final != "00/00/0000 00:00:00" && $data_inicial != "0000-00-00 00:00:00" && $data_final != "0000-00-00 00:00:00") {

        $diferenca = strtotime($data_final) - strtotime($data_inicial);

        $dias = floor($diferenca / (60 * 60 * 24));

        if ($dias > 1) {
            return $dias;
        } else if ($dias == 1) {
            return $dias;
        } else if ($dias == 0) {
            return $dias;
        } else if ($dias < 0) {
            return 0;
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
function formata_data($data) {
    if ($data == '')
        return '';
    $d = explode('/', $data);
    return $d[2] . '-' . $d[1] . '-' . $d[0];
}

//------------------------------------------------------------------------------
function data_volta($data) {
    if ($data == '' || $data == '0000-00-00')
        return '';
    $d = explode('-', $data);
    return $d[2] . '/' . $d[1] . '/' . $d[0];
}

//------------------------------------------------------------------------------
function hora($hora) { // Deixa a hora 20:00
    $h = explode(':', $hora);
    return $h[0] . ':' . $h[1];
}

//------------------------------------------------------------------------------
function getSemana($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'SEG';
            $comp = 'Segunda-feira';
            break;
        case 2 :
            $r = 'TER';
            $comp = 'Terça-feira';
            break;
        case 3 :
            $r = 'QUA';
            $comp = 'Quarta-feira';
            break;
        case 4 :
            $r = 'QUI';
            $comp = 'Quinta-feira';
            break;
        case 5 :
            $r = 'SEX';
            $comp = 'Sexta-feira';
            break;
        case 6 :
            $r = 'SAB';
            $comp = 'Sábado';
            break;
        case 7 :
            $r = 'DOM';
            $comp = 'Domingo';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function getSemana2($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'Seg';
            $comp = 'Segunda-feira';
            break;
        case 2 :
            $r = 'Ter';
            $comp = 'Terça-feira';
            break;
        case 3 :
            $r = 'Qua';
            $comp = 'Quarta-feira';
            break;
        case 4 :
            $r = 'Qui';
            $comp = 'Quinta-feira';
            break;
        case 5 :
            $r = 'Sex';
            $comp = 'Sexta-feira';
            break;
        case 6 :
            $r = 'Sab';
            $comp = 'Sábado';
            break;
        case 7 :
            $r = 'Dom';
            $comp = 'Domingo';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function getDiaSemana($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'Dom';
            $comp = 'Domingo';
            break;
        case 2 :
            $r = 'Seg';
            $comp = 'Segunda-feira';
            break;
        case 3 :
            $r = 'Ter';
            $comp = 'Terça-feira';
            break;
        case 4 :
            $r = 'Qua';
            $comp = 'Quarta-feira';
            break;
        case 5 :
            $r = 'Qui';
            $comp = 'Quinta-feira';
            break;
        case 6 :
            $r = 'Sex';
            $comp = 'Sexta-feira';
            break;
        case 7 :
            $r = 'Sab';
            $comp = 'Sábado';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function hoje($data) {
    $dt = explode('/', $data);
    return getSemana(date("N", mktime(0, 0, 0, $dt[1], $dt[0], intval($dt[2]))), 1);
}

//------------------------------------------------------------------------------
function timeDiff($firstTime, $lastTime) {
    $firstTime = strtotime($firstTime);
    $lastTime = strtotime($lastTime);
    $timeDiff = $lastTime - $firstTime;
    return $timeDiff;
}

//------------------------------------------------------------------------------
function separa_hora($hora, $op) { // $op = minutos = 1; hora = 0
    $hr = explode(':', $hora);
    return $hr[$op];
}

//------------------------------------------------------------------------------
function dataExtenso($dt) {
    $da = explode('/', $dt);
    return $da[0] . ' de ' . getMes($da[1]) . ' de ' . $da[2];
}

//------------------------------------------------------------------------------
function dataExtensoTimeline($dt) {
    $da = explode('/', $dt);
    $diasemana = date("w", mktime(0, 0, 0, $da[1], $da[0], $da[2]));
    return getSemana2($diasemana, 0) . '  ' . getMes2($da[1]) . '  ' . $da[0] . ' ' . $da[2];
}

//------------------------------------------------------------------------------
function getMes($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "Janeiro";
            break;
        case 2 :
            $mes = "Fevereiro";
            break;
        case 3 :
            $mes = "Março";
            break;
        case 4 :
            $mes = "Abril";
            break;
        case 5 :
            $mes = "Maio";
            break;
        case 6 :
            $mes = "Junho";
            break;
        case 7 :
            $mes = "Julho";
            break;
        case 8 :
            $mes = "Agosto";
            break;
        case 9 :
            $mes = "Setembro";
            break;
        case 10 :
            $mes = "Outubro";
            break;
        case 11 :
            $mes = "Novembro";
            break;
        case 12 :
            $mes = "Dezembro";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes2($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "Jan";
            break;
        case 2 :
            $mes = "Fev";
            break;
        case 3 :
            $mes = "Mar";
            break;
        case 4 :
            $mes = "Abr";
            break;
        case 5 :
            $mes = "Mai";
            break;
        case 6 :
            $mes = "Jun";
            break;
        case 7 :
            $mes = "Jul";
            break;
        case 8 :
            $mes = "Ago";
            break;
        case 9 :
            $mes = "Set";
            break;
        case 10 :
            $mes = "Out";
            break;
        case 11 :
            $mes = "Nov";
            break;
        case 12 :
            $mes = "Dez";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes3($m) {
    switch ($m) {
        case 1 :
            $mes = "janeiro";
            break;
        case 2 :
            $mes = "fevereiro";
            break;
        case 3 :
            $mes = "marco";
            break;
        case 4 :
            $mes = "abril";
            break;
        case 5 :
            $mes = "maio";
            break;
        case 6 :
            $mes = "junho";
            break;
        case 7 :
            $mes = "julho";
            break;
        case 8 :
            $mes = "agosto";
            break;
        case 9 :
            $mes = "setembro";
            break;
        case 10 :
            $mes = "outubro";
            break;
        case 11 :
            $mes = "novembro";
            break;
        case 12 :
            $mes = "dezembro";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes4($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "JANEIRO";
            break;
        case 2 :
            $mes = "FEVEREIRO";
            break;
        case 3 :
            $mes = "MARÇO";
            break;
        case 4 :
            $mes = "ABRIL";
            break;
        case 5 :
            $mes = "MAIO";
            break;
        case 6 :
            $mes = "JUNHO";
            break;
        case 7 :
            $mes = "JULHO";
            break;
        case 8 :
            $mes = "AGOSTO";
            break;
        case 9 :
            $mes = "SETEMBRO";
            break;
        case 10 :
            $mes = "OUTUBRO";
            break;
        case 11 :
            $mes = "NOVEMBRO";
            break;
        case 12 :
            $mes = "DEZEMBRO";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function ctexto($texto, $frase = 'pal') {
    switch ($frase) {
        case 'fra' : // Apenas a a primeira letra em maiusculo
            $texto = ucfirst(mb_strtolower($texto));
            break;
        case 'min' :
            $texto = mb_strtolower($texto);
            break;
        case 'mai' :
            $texto = colocaAcentoMaiusculo((mb_strtoupper($texto)));
            break;
        case 'pal' : // Todas as palavras com a primeira em maiusculo
            $texto = ucwords(mb_strtolower($texto));
            break;
        case 'pri' : // Todos os primeiros caracteres de cada palavra em maiusuclo, menos as junções
            $texto = titleCase($texto);
            break;
    }
    return $texto;
}

//------------------------------------------------------------------------------
function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI")) {
    /*
     * Exceptions in lower case are words you don't want converted
     * Exceptions all in upper case are any words you don't want converted to title case
     * but should be converted to upper case, e.g.:
     * king henry viii or king henry Viii should be King Henry VIII
     */
    $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    foreach ($delimiters as $dlnr => $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $wordnr => $word) {
            if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtoupper($word, "UTF-8");
            } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtolower($word, "UTF-8");
            } elseif (!in_array($word, $exceptions)) {
                // convert to uppercase (non-utf8 only)
                $word = ucfirst($word);
            }
            array_push($newwords, $word);
        }
        $string = join($delimiter, $newwords);
    } // foreach
    return $string;
}

//------------------------------------------------------------------------------
function colocaAcentoMaiusculo($texto) {
    $array1 = array(
        "á",
        "à",
        "â",
        "ã",
        "ä",
        "é",
        "è",
        "ê",
        "ë",
        "í",
        "ì",
        "î",
        "ï",
        "ó",
        "ò",
        "ô",
        "õ",
        "ö",
        "ú",
        "ù",
        "û",
        "ü",
        "ç"
    );

    $array2 = array(
        "Á",
        "À",
        "Â",
        "Ã",
        "Ä",
        "É",
        "È",
        "Ê",
        "Ë",
        "Í",
        "Ì",
        "Î",
        "Ï",
        "Ó",
        "Ò",
        "Ô",
        "Õ",
        "Ö",
        "Ú",
        "Ù",
        "Û",
        "Ü",
        "Ç"
    );
    return str_replace($array1, $array2, $texto);
}

//------------------------------------------------------------------------------
function retira_acentos($texto) {
    $array1 = array(
        "á",
        "à",
        "â",
        "ã",
        "ä",
        "é",
        "è",
        "ê",
        "ë",
        "í",
        "ì",
        "î",
        "ï",
        "ó",
        "ò",
        "ô",
        "õ",
        "ö",
        "ú",
        "ù",
        "û",
        "ü",
        "ç",
        "Á",
        "À",
        "Â",
        "Ã",
        "Ä",
        "É",
        "È",
        "Ê",
        "Ë",
        "Í",
        "Ì",
        "Î",
        "Ï",
        "Ó",
        "Ò",
        "Ô",
        "Õ",
        "Ö",
        "Ú",
        "Ù",
        "Û",
        "Ü",
        "Ç"
    );
    $array2 = array(
        "a",
        "a",
        "a",
        "a",
        "a",
        "e",
        "e",
        "e",
        "e",
        "i",
        "i",
        "i",
        "i",
        "o",
        "o",
        "o",
        "o",
        "o",
        "u",
        "u",
        "u",
        "u",
        "c",
        "A",
        "A",
        "A",
        "A",
        "A",
        "E",
        "E",
        "E",
        "E",
        "I",
        "I",
        "I",
        "I",
        "O",
        "O",
        "O",
        "O",
        "O",
        "U",
        "U",
        "U",
        "U",
        "C"
    );
    return str_replace($array1, $array2, $texto);
}

//------------------------------------------------------------------------------
// Cria uma função que retorna o timestamp de uma data no formato DD/MM/AAAA
function geraTimestamp($data) {
    $partes = explode('/', $data);
    return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
}

//------------------------------------------------------------------------------
function obterDataBRTimestamp($data) {
    if ($data != '') {
        $data = substr($data, 0, 10);
        $explodida = explode("-", $data);
        $dataIso = $explodida[2] . "/" . $explodida[1] . "/" . $explodida[0];
        return $dataIso;
    }
    return NULL;
}

//------------------------------------------------------------------------------
function convertDataBR2ISO($data) {
    if ($data == '')
        return false;
    $explodida = explode("/", $data);
    $dataIso = $explodida[2] . "-" . $explodida[1] . "-" . $explodida[0];
    return $dataIso;
}

//------------------------------------------------------------------------------
function obterHoraTimestamp($data) {
    return substr($data, 11, 5);
}

//------------------------------------------------------------------------------
function obterDiaTimestamp($data) {
    return substr($data, 8, 2);
}

//------------------------------------------------------------------------------
function obterMesTimestamp($data) {
    return substr($data, 5, 2);
}

//------------------------------------------------------------------------------
function obterAnoTimestamp($data) {
    return substr($data, 0, 4);
}

//------------------------------------------------------------------------------
function calculaDiferencaDatas($data_inicial, $data_final) {
    // Usa a função criada e pega o timestamp das duas datas:
    $time_inicial = geraTimestamp($data_inicial);
    $time_final = geraTimestamp($data_final);

    // Calcula a diferença de segundos entre as duas datas:
    $diferenca = $time_final - $time_inicial; // 19522800 segundos
    // Calcula a diferença de dias
    $dias = (int) floor($diferenca / (60 * 60 * 24)); // 225 dias
    // Exibe uma mensagem de resultado:
    // echo "A diferença entre as datas ".$data_inicial." e ".$data_final." é de <strong>".$dias."</strong> dias";
    return $dias;
}

//------------------------------------------------------------------------------
function apelidometadatos($variavel) {
    /*
     * $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ ,;:./';
     * $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr______';
     * //$string = ($string);
     * $string = strtr($string, ($a), $b); //substitui letras acentuadas por "normais"
     * $string = str_replace(" ","",$string); // retira espaco
     * $string = strtolower($string); // passa tudo para minusculo
     */
    $string = strtolower(ereg_replace("[^a-zA-Z0-9-]", "-", strtr((trim($variavel)), ("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"), "aaaaeeiooouuncAAAAEEIOOOUUNC-")));
    return ($string); // finaliza, gerando uma saída para a funcao
}

//------------------------------------------------------------------------------
function getExtensaoArquivo($extensao) {
    switch ($extensao) {
        case 'image/jpeg' :
            $ext = ".jpeg";
            break;
        case 'image/jpg' :
            $ext = ".jpg";
            break;
        case 'image/pjpeg' :
            $ext = ".pjpg";
            break;
        case 'image/JPEG' :
            $ext = ".JPEG";
            break;
        case 'image/gif' :
            $ext = ".gif";
            break;
        case 'image/png' :
            $ext = ".png";
            break;
        case 'video/webm' :
            $ext = ".webm";
            break;
        case 'video/mp4' :
            $ext = ".mp4";
            break;
        case 'video/flv' :
            $ext = ".flv";
            break;
        case 'video/webm' :
            $ext = ".webm";
            break;
        case 'audio/mp4' :
            $ext = ".acc";
            break;
        case 'audio/mpeg' :
            $ext = ".mp3";
            break;
        case 'audio/ogg' :
            $ext = ".ogg";
            break;
    }
    return $ext;
}

//------------------------------------------------------------------------------
function uploadArquivoPermitido($arquivo) {
    $tiposPermitidos = array(
        'image/gif',
        'image/jpeg',
        'image/jpg',
        'image/pjpeg',
        'image/png',
        'video/webm',
        'video/mp4',
        'video/ogv',
        'audio/mp3',
        'audio/mp4',
        'audio/mpeg',
        'audio/ogg'
    );
    if (array_search($arquivo, $tiposPermitidos) === false) {
        return false;
    } else {
        return true;
    } // end if
}

//------------------------------------------------------------------------------
function converteValorMonetario($valor) {
    $valor = str_replace('.', '', $valor);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace(',', '.', $valor);
    return $valor;
}

//------------------------------------------------------------------------------
function valorMonetario($valor) {
    $valor = number_format($valor, 2, ',', '.');
    return $valor;
}

//------------------------------------------------------------------------------
function real2float($num) {
    $num = str_replace(".", "", $num);
    $num = str_replace(",", ".", $num);
    return $num;
}

//------------------------------------------------------------------------------
function desconto($valor, $porcentagem) {
    $desconto = ($porcentagem / 100) * $valor;

    $rs = $valor - $desconto;

    return $rs;
}

//------------------------------------------------------------------------------
function somar_desconto($valor, $porcentagem) {
    $desconto = ($porcentagem / 100) * $valor;

    $rs = $valor + $desconto;

    return $rs;
}

//------------------------------------------------------------------------------
function fdec($numero, $formato = NULL, $tmp = NULL) {
    switch ($formato) {
        case null :
            if ($numero != 0)
                $numero = number_format($numero, 2, ',', '.');
            else
                $numero = '0,00';
            break;
        case '%' :
            if ($numero > 0)
                $numero = number_format((($numero / $tmp) * 100), 2, ',', '.') . '%';
            else
                $numero = '0%';
            break;
        case '-' :
            $numero = "<font color='red'>" . fdec($numero) . "</font>";
            break;
    }
    return $numero;
}

//------------------------------------------------------------------------------
function verificarloginduplicado($usuario, $idsessao, $query) {
    $oConexao = Conexao::getInstance();
    $retorno = true;
    $querysessao = $oConexao->query($query);
    $qtdsessao = $querysessao->rowCount();
    if ($qtdsessao == 0) {
        $retorno = false;
    }
    return $retorno;
}

//------------------------------------------------------------------------------
// FUNÇÃO QUE CAPTURA O VALOR DE UM ARRAY E IGNORA OS VALORES VAZIOS
function pegar_valor_array($valor_array) {
    $rs = "";

    foreach ($valor_array as $key => $value) {
        if ($value != "" && $value != NULL && $value != "0" && $value != "undefined") {
            $rs = $value;
        }
    }
    return $rs;
}

//------------------------------------------------------------------------------
function romano($N) {
    $N1 = $N;
    $Y = "";
    while ($N / 1000 >= 1) {
        $Y .= "M";
        $N = $N - 1000;
    }
    if ($N / 900 >= 1) {
        $Y .= "CM";
        $N = $N - 900;
    }
    if ($N / 500 >= 1) {
        $Y .= "D";
        $N = $N - 500;
    }
    if ($N / 400 >= 1) {
        $Y .= "CD";
        $N = $N - 400;
    }
    while ($N / 100 >= 1) {
        $Y .= "C";
        $N = $N - 100;
    }
    if ($N / 90 >= 1) {
        $Y .= "XC";
        $N = $N - 90;
    }
    if ($N / 50 >= 1) {
        $Y .= "L";
        $N = $N - 50;
    }
    if ($N / 40 >= 1) {
        $Y .= "XL";
        $N = $N - 40;
    }
    while ($N / 10 >= 1) {
        $Y .= "X";
        $N = $N - 10;
    }
    if ($N / 9 >= 1) {
        $Y .= "IX";
        $N = $N - 9;
    }
    if ($N / 5 >= 1) {
        $Y .= "V";
        $N = $N - 5;
    }
    if ($N / 4 >= 1) {
        $Y .= "IV";
        $N = $N - 4;
    }
    while ($N >= 1) {
        $Y .= "I";
        $N = $N - 1;
    }
    return $Y;
}

//------------------------------------------------------------------------------
function valorPorExtenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false) {
    $singular = null;
    $plural = null;

    if ($bolExibirMoeda) {
        $singular = array(
            "centavo",
            "real",
            "mil",
            "milhão",
            "bilhão",
            "trilhão",
            "quatrilhão"
        );
        $plural = array(
            "centavos",
            "reais",
            "mil",
            "milhões",
            "bilhões",
            "trilhões",
            "quatrilhões"
        );
    } else {
        $singular = array(
            "",
            "",
            "mil",
            "milhão",
            "bilhão",
            "trilhão",
            "quatrilhão"
        );
        $plural = array(
            "",
            "",
            "mil",
            "milhões",
            "bilhões",
            "trilhões",
            "quatrilhões"
        );
    }

    $c = array(
        "",
        "cem",
        "duzentos",
        "trezentos",
        "quatrocentos",
        "quinhentos",
        "seiscentos",
        "setecentos",
        "oitocentos",
        "novecentos"
    );
    $d = array(
        "",
        "dez",
        "vinte",
        "trinta",
        "quarenta",
        "cinquenta",
        "sessenta",
        "setenta",
        "oitenta",
        "noventa"
    );
    $d10 = array(
        "dez",
        "onze",
        "doze",
        "treze",
        "quatorze",
        "quinze",
        "dezesseis",
        "dezesete",
        "dezoito",
        "dezenove"
    );
    $u = array(
        "",
        "um",
        "dois",
        "três",
        "quatro",
        "cinco",
        "seis",
        "sete",
        "oito",
        "nove"
    );

    if ($bolPalavraFeminina) {

        if ($valor == 1) {
            $u = array(
                "",
                "uma",
                "duas",
                "três",
                "quatro",
                "cinco",
                "seis",
                "sete",
                "oito",
                "nove"
            );
        } else {
            $u = array(
                "",
                "um",
                "duas",
                "três",
                "quatro",
                "cinco",
                "seis",
                "sete",
                "oito",
                "nove"
            );
        }

        $c = array(
            "",
            "cem",
            "duzentas",
            "trezentas",
            "quatrocentas",
            "quinhentas",
            "seiscentas",
            "setecentas",
            "oitocentas",
            "novecentas"
        );
    }

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);

    for ($i = 0; $i < count($inteiro); $i++) {
        for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
            $inteiro[$i] = "0" . $inteiro[$i];
        }
    }

    // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
    $rt = null;
    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000")
            $z++;
        elseif ($z > 0)
            $z--;

        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];

        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    $rt = mb_substr($rt, 1);

    return ($rt ? trim($rt) : "zero");
}

//------------------------------------------------------------------------------
function mask($val, $mask) {
    $maskared = '';
    $k = 0;
    for ($i = 0; $i <= strlen($mask) - 1; $i++) {
        if ($mask[$i] == '#') {
            if (isset($val[$k])) {
                $maskared .= $val[$k++];
            }
        } else {
            if (isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
    }
    return $maskared;
}

//------------------------------------------------------------------------------
// retorna diferença em horas
function retorna_dif_horas($data) {
    $hora_data_atual = date("Y-m-d H:i:s");
    $data = strtotime($data);
    $hora_data_atual = strtotime($hora_data_atual);
    $diferenca = $hora_data_atual - $data;
    $horas = floor($diferenca / 3600);
    return $horas;
}

//------------------------------------------------------------------------------
function removeAcentos($string) {
    return preg_replace(array("/(ç)/", "/(Ç)/", "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "c C a A e E i I o O u U n N"), $string);
}

//------------------------------------------------------------------------------
?>