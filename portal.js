//------------------------------------------------------------------------------
$(document).ready(function () {
    $(".camera_prev").hide();
    $(".camera_next").hide();
});
//------------------------------------------------------------------------------
$('body').mouseover(function () {
    $("div#car_search").slideUp("slow");
});
//------------------------------------------------------------------------------
$('div#div_modal_car').mouseover(function () {
    event.stopPropagation();
});
//------------------------------------------------------------------------------
$("ul#icone_carrinho").mouseover(function () {
    $("div#car_search").slideDown("slow");
    event.stopPropagation();
});
//------------------------------------------------------------------------------
function deslizar(op) {
    if (op == 0) {
        var topPosition = $("body").offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    } else if (op == 1) {
        var topPosition = $("#menu_produto").offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
}
//------------------------------------------------------------------------------
