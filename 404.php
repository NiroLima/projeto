<?php
include_once('conf/config.php');
include_once('conf/Url.php');

$db = Conexao::getInstance();
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="application-name" content="&nbsp;"/>
        <meta name="theme-color" content="#000000">
        <title>E-COMMERCE</title>

        <link rel="shortcut icon" type="image/x-icon" href="<?= PORTAL_URL; ?>assets/img/9072logotelles.ico">

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,600,500,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Belleza' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>

        <link href="<?= PORTAL_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/fontawesome/4.2.0/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/geral.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/carousel.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/global.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/styleecba.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/media.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?= PORTAL_URL; ?>assets/css/wishlis.css" rel="stylesheet" type="text/css" media="all" />

    </head>

    <body class="templateIndex">


        <br/>  <br/>  <br/>  <br/>  <br/>  <br/>  <br/>

        <div class="wrapper-page">
            <div class="card">
                <div class="card-block">
                    <div class="ex-page-content text-center">
                        <h1 class="">404!</h1>
                        <h4 class="">Desculpe, página não encontrada</h4>
                        <br><a class="btn btn-info mb-5 waves-effect waves-light" href="javascript:window.history.go(-1)"><i class="mdi mdi-home"></i> Voltar</a></div>
                </div>
            </div>
        </div>

        <br/>  <br/>  <br/>  <br/>  <br/>  <br/>  <br/>

    </body>
</html>