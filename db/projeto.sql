-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Ago-2021 às 17:25
-- Versão do servidor: 10.4.19-MariaDB
-- versão do PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projeto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedores`
--

CREATE TABLE `fornecedores` (
  `id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) DEFAULT NULL,
  `razao_social` varchar(300) DEFAULT NULL,
  `cpf` varchar(30) DEFAULT NULL,
  `cnpj` varchar(30) DEFAULT NULL,
  `contato` varchar(30) DEFAULT NULL,
  `endereco` varchar(300) DEFAULT NULL,
  `responsavel_id` int(11) DEFAULT NULL,
  `data_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fornecedores`
--

INSERT INTO `fornecedores` (`id`, `nome_fantasia`, `razao_social`, `cpf`, `cnpj`, `contato`, `endereco`, `responsavel_id`, `data_update`, `data_cadastro`, `status`) VALUES
(2, 'DÍNIA A. V. AIACHE', 'DÍNIA A. V. AIACHE', NULL, '15.546.579/0001-60', '(13)1232-1312', 'Av. Ceará, nº 1059, Bairro Centro, CEP: 69.900-088 – Rio Branco/AC', 1, '2021-08-11 13:38:11', '2021-05-17 22:42:55', 1),
(3, 'ORTIZ TÁXI AÉREO LTDA', NULL, '132.131.231-23', NULL, '(68)3211-1048', 'BR 364, KM 18, s/n, Zona Rural, Aeroporto Internacional de Rio Branco, CEP: 69.914-220 – Rio Branco/AC', 1, '2021-08-13 00:45:39', '2021-05-21 02:31:35', 1),
(4, 'LIMA E ABRAHÃO LTDA', 'LIMA E ABRAHÃO LTDA', NULL, '84.308.337/0001-50', '(13)2132-1312', 'Av. Getúlio Vargas, nº 3457, Vila Ivonete, CEP: 69.918-616, nesta cidade de Rio Branco- Acre', 7, '2021-08-11 01:05:26', '2021-06-09 21:45:32', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `foto` varchar(300) DEFAULT NULL,
  `nome` varchar(300) NOT NULL,
  `codigo` varchar(30) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  `fornecedor_id` int(11) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `valor` decimal(15,2) DEFAULT NULL,
  `responsavel_id` int(11) DEFAULT NULL,
  `data_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `foto`, `nome`, `codigo`, `descricao`, `fornecedor_id`, `qtd`, `valor`, `responsavel_id`, `data_update`, `data_cadastro`, `status`) VALUES
(1, 'assets/img_produtos/fogao.jpg', 'Fogão', '01', 'Medindo 08x08x03 cm; (Caixinha em madeira com tampa trabalhada em Marchetaria com motivos de: Borboletas, peles, penas, flores, folhas costela de adão ou geométricos).', 3, 10, '150.00', 1, '2021-08-13 00:45:45', '2021-05-12 20:09:35', 1),
(5, 'assets/img_produtos/xbox.jpg', 'Xbox', '13', 'Coroa de flores naturais em rosas e flores do campo (margaridas e/ou crisântemos); no tamanho 1,00m x 0,80m com faixa de homenagens póstumas; homenagens póstumas do governo do estado do acre; quarenta e uma letras / 8 palavras', 2, 10, '690.00', 1, '2021-08-13 02:10:42', '2021-05-17 22:45:07', 1),
(6, 'assets/img_produtos/celular.jpg', 'Galaxy S21', '03', 'Contratação de empresa especializada em serviços de fretamento de aeronaves em trechos nacionais (interestadual e intermunicipal) e internacionais, visando atender as necessidades do Governador e Secretaria de Estado da Casa Civil', 3, 11, '83.34', 1, '2021-08-11 14:56:19', '2021-05-21 03:01:13', 1),
(9, 'assets/img_produtos/notebook.jpg', 'Notebook', '31', 'asdas sas das', 4, 123, '5.00', 1, '2021-08-11 14:13:40', '2021-08-11 00:27:12', 1),
(10, 'assets/img_produtos/50420210812210712.cut.png', 'Lavadora Electrolux LAC13', '132', 'Lavadora Electrolux LAC13 13 Kg Branca - 110v - A Lavadora Electrolux LAC13 conta com tecnologias para trazer muito mais praticidade para o dia a dia na hora da lavagem. Com tecnologia autolimpante Jet&Clean deixa o dispenser perfeito para o próximo ciclo', 4, 12, '1599.99', 1, '2021-08-13 02:08:58', '2021-08-13 02:02:46', 1),
(11, 'assets/img_produtos/69120210812211539.cut.png', 'Smart TV Android LED', '313', 'Smart TV Android LED 32\" Semp 32S5300 Bluetooth 2 HDMI 1 USB Controle Remoto com Comando de Voz e Google Assistant - Descubra uma emocionante experiência e uma maneira inteligente de aproveitar sua TV. Inteligência é ter o controle da sua TV. A S5300 é a evolução de como você vai ver, ouvir e interagir com a sua programação favorita.', 3, 32, '1357.00', 1, '2021-08-13 02:15:46', '2021-08-13 02:15:46', 1),
(12, NULL, 'Refrigerador Brastemp Side Inverse BRO80', '31312', 'Refrigerador Brastemp Side Inverse BRO80 540 Litros Ice Maker Evox 110v - Brastemp Side Inverse alia sofisticação, alta capacidade e praticidade. A Brastemp apresenta para você uma nova geração de geladeiras. A Side Inverse é compacta por fora e, por dentro dentro, tem uma capacidade de armazenamento.', 4, 5, '6200.00', 1, '2021-08-13 02:17:55', '2021-08-13 02:17:55', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `seg_usuario`
--

CREATE TABLE `seg_usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(130) CHARACTER SET latin1 DEFAULT NULL,
  `login` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `senha` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `data_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `contato` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `seg_usuario`
--

INSERT INTO `seg_usuario` (`id`, `nome`, `login`, `senha`, `data_update`, `data_cadastro`, `status`, `contato`) VALUES
(1, 'Administrador', 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2021-04-08 21:18:03', '2019-07-13 07:43:18', 1, '(68)9999-9999'),
(7, 'Fernando de Silva Lima', 'fernando.lima', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '2021-08-11 13:38:05', '2021-08-10 22:34:52', 1, '(68)9999-9999');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fornecedores_usuario_id` (`responsavel_id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_fornecedor_id` (`fornecedor_id`),
  ADD KEY `produtos_usuario_id` (`responsavel_id`);

--
-- Índices para tabela `seg_usuario`
--
ALTER TABLE `seg_usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `seg_usuario`
--
ALTER TABLE `seg_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD CONSTRAINT `fornecedores_usuario_id` FOREIGN KEY (`responsavel_id`) REFERENCES `seg_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_fornecedor_id` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `produtos_usuario_id` FOREIGN KEY (`responsavel_id`) REFERENCES `seg_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
