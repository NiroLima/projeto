<?php include_once('template/header.php'); ?>

<div id="content-wrapper-parent">
    <div id="content-wrapper">  
        <div class="home-slider-wrapper clearfix">
            <div class="camera_wrap" id="home-slider">
                <div data-src="<?= PORTAL_URL; ?>assets/img/banner/banner1.png">
                    <div class="camera_caption camera_title_1 fadeIn hidden-xs">
                        <h1>Melhores eletrodomésticos com preços abaixo do valor de mercado!</h1>
                    </div>
                    <div class="camera_caption camera_caption_1 fadeIn" style="color: white;">
                        Conheça nossa coleção de eletrodomésticos.
                    </div>
                    <div class="camera_cta_1">
                        <a href="<?= PORTAL_URL ?>detalhes" class="btn">Ver Coleção</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content -->
        <div id="content" class="clearfix">        
            <section class="content">
                <div id="col-main" class="clearfix">
                    <div class="home-popular-collections">
                        <div class="container">
                            <div class="group_home_collections row">
                                <div class="col-md-24">
                                    <div id="menu_produto" class="home_collections">
                                        <h6 class="general-title">Produtos</h6>
                                        <div class="home_collections_wrapper">
                                            <div id="home_collections">
                                                <?php
                                                $result = $db->prepare("SELECT p.id, p.nome, p.foto    
                                                      FROM produtos p 
                                                      WHERE p.status = 1
                                                      ORDER BY p.nome");
                                                $result->execute();
                                                while ($produtos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>

                                                    <div class="home_collections_item">
                                                        <div class="home_collections_item_inner">
                                                            <div class="collection-details">
                                                                <a href="<?= PORTAL_URL . "detalhes/" . $produtos['id'] ?>" title="<?= $produtos['nome'] ?>">
                                                                    <img style="width: 250px; height: 250px;" src="<?= PORTAL_URL . "" . ($produtos['foto'] != "" ? $produtos['foto'] : "assets/img_produtos/sem_imagem.png") ?>" alt="<?= $produtos['nome'] ?>" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="hover-overlay text-center">
                                                            <span class="col-name"><a href="<?= PORTAL_URL . "detalhes/" . $produtos['id'] ?>"><?= $produtos['nome'] ?></a></span>
                                                            <div class="collection-action">
                                                                <a href="<?= PORTAL_URL . "detalhes/" . $produtos['id'] ?>">Veja Mais</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>        
        </div>
    </div>
</div>

<?php include_once('template/footer.php'); ?>

<!-- JS DA PÁGINA PORTAL -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>portal.js"></script>