<?php

header("Cache-Control: max-age=300, must-revalidate"); // DEFINIR TIMEZONE PADRÃO

date_default_timezone_set("Brazil/Acre");

// DEFININDO OS DADOS DE ACESSO AO BANCO DE DADOS
define("DB", 'mysql');
define("DB_HOST", "localhost");
define("DB_NAME", "projeto");
define("DB_USER", "root");
define("DB_PASS", "");

// CONFIGURACOES PADRAO DO SISTEMA
define("PORTAL_URL", 'http://localhost/projeto/');
define("TITULOSISTEMA", 'E-COMMERCE');
define("LOGO_FOLDER", 'http://localhost/projeto/assets/images/logo_dark.png');
define("CSS_FOLDER", 'http://localhost/projeto/assets/css/');
define("IMG_FOLDER", 'http://localhost/projeto/assets/img/');
define("JS_FOLDER", 'http://localhost/projeto/assets/js/');
define("FONTS_FOLDER", 'http://localhost/projeto/assets/fontes/');
define("PLUGINS_FOLDER", 'http://localhost/projeto/assets/plugins/');
define("UTILS_FOLDER", 'http://localhost/projeto/utils/');
define("ASSETS_FOLDER", 'http://localhost/projeto/assets/');
define("PORTAL_URL_SERVIDOR", 'http://localhost/projeto/');

// ADICIONAR CLASSE DE CONEÇÃO
include_once ("Conexao.class.php");
?>