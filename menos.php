<?php

require($_SERVER['DOCUMENT_ROOT'] . '/projeto/classes/carrinho.php');

$db = Conexao::getInstance();

$id = @$_POST['id'];

$cart = new Cart([
    // limitar quantidade items no carrinho
    'cartMaxItem' => 0,
    // setar o máximo de quantidade de items no carrinho
    'itemMaxQuantity' => 99,
    'useCookie' => true,
        ]);

$resultado = $cart->lookItem($id, "quantity") - 1;

$resultado = $resultado >= 0 ? $resultado : 0;

$cart->update($id, $resultado);

//MENSAGEM DE SUCESSO
$msg['id'] = $id;
$msg['msg'] = 'success';
$msg['retorno'] = 'Produto atualizado com sucesso!';
echo json_encode($msg);
exit();
?>
    
