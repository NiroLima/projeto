<?php

include_once('conf/config.php');
$db = Conexao::getInstance();

$msg = array();

try {
    //PEGAR DADOS DE LOGIN
    $login = strip_tags($_POST['login']);
    $senha = strip_tags(sha1($_POST['senha']));
    //SQL PARA VERIFICAÇÃO DE LOGIN EXISTENTE
    $result = $db->prepare("SELECT *  
                            FROM seg_usuario 
                            WHERE login = ?");
    $result->bindParam(1, $login);
    $result->execute();
    $num = $result->rowCount();

    if ($num > 0) {
        //PEGA OS DADOS DO USUARIO, CASO TENHA ACESSO
        $dadosUsuario = $result->fetch(PDO::FETCH_ASSOC);

        //VERIFICA SE A SENHA INFORMADA É IGUAL DO USUARIO
        if ($senha == $dadosUsuario['senha']) {

            if ($dadosUsuario['status'] == 1) {

                $id = $dadosUsuario['id'];
                //CRIAR O TIMEOUT DA SESSÃO PARA EXPIRAR
                $_SESSION['timeout'] = time();
                //CRIAR AS SESSÕES DO USUARIO
                $_SESSION['id'] = $id;
                $_SESSION['nome'] = $dadosUsuario['nome'];
                $_SESSION['login'] = $login;

                //MENSAGEM DE SUCESSO
                $msg['id'] = $id;
                $msg['msg'] = 'success';
                $msg['retorno'] = 'Login efetuado com sucesso.';
                echo json_encode($msg);
                exit();
            } else {
                $msg['msg'] = 'error';
                $msg['retorno'] = 'Você não tem permissão de acesso ao sistema.';
                echo json_encode($msg);
                exit();
            }
        } else {
            $msg['msg'] = 'error';
            $msg['retorno'] = 'O usuário ou a senha inseridos estão incorretos.';
            echo json_encode($msg);
            exit();
        }
    } else {
        $msg['msg'] = 'error';
        $msg['retorno'] = 'O usuário ou a senha inseridos estão incorretos.';
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar efeturar o login. :" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>