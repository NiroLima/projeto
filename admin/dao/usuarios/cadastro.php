
<?php

$db = Conexao::getInstance();

$usuario_id = (@$_POST['id']);
$usuario_nome = isset($_POST['nome']) && $_POST['nome'] != "" ? (@$_POST['nome']) : null;
$usuario_login = isset($_POST['login']) && $_POST['login'] != "" ? (@$_POST['login']) : null;
$usuario_senha = isset($_POST['senha']) && $_POST['senha'] != "" ? (@$_POST['senha']) : null;
$usuario_contato = isset($_POST['contato']) && $_POST['contato'] != "" ? @$_POST['contato'] : null;

$error = false;

$mensagem = "";

$msg = array();

try {

    //VERIFICA SE O NOME DO USUÁRIO JÁ FOI INFORMADO
    $id_nome = pesquisar("id", "seg_usuario", "nome", "=", $usuario_nome, "");
    if (is_numeric($id_nome) && $id_nome != @$_POST['id']) {
        $error = true;
        $mensagem .= "<span>O nome do usuário informado já existe no sistema.</span>";
        $msg['tipo'] = "nome";
    }
    //VERIFICA SE O LOGIN DO USUÁRIO JÁ FOI INFORMADO
    $id_login = pesquisar("id", "seg_usuario", "login", "=", $usuario_login, "");
    if (is_numeric($id_login) && $id_login != @$_POST['id']) {
        $error = true;
        $mensagem .= "<span>O login do usuário informado já existe no sistema.</span>";
        $msg['tipo'] = "login";
    }

    if ($error == false) {

        $db->beginTransaction();

        if (isset($_POST['id']) && @$_POST['id'] != '') {//CASO O USUÁRIO JÁ ESTEJA CADASTRADO
            if ($usuario_senha != "" && $usuario_senha != null) {
                $stmt = $db->prepare("UPDATE seg_usuario set nome = ?, login = ?, contato = ?, senha = ? WHERE id = ?");
                $stmt->bindValue(1, ($usuario_nome));
                $stmt->bindValue(2, $usuario_login);
                $stmt->bindValue(3, $usuario_contato);
                $stmt->bindValue(4, sha1($usuario_senha));
                $stmt->bindValue(5, $_POST['id']);

                $stmt->execute();
            } else {
                $stmt = $db->prepare("UPDATE seg_usuario set nome = ?, login = ?, contato = ? WHERE id = ?");
                $stmt->bindValue(1, ($usuario_nome));
                $stmt->bindValue(2, $usuario_login);
                $stmt->bindValue(3, $usuario_contato);
                $stmt->bindValue(4, $_POST['id']);

                $stmt->execute();
            }

            $usuario_id = $_POST['id'];

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $usuario_id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O usuário foi atualizado.';
            echo json_encode($msg);
            exit();
        } else {//CASO O USUÁRIO AINDA NÃO ESTEJA CADASTRADO
            $stmt = $db->prepare("INSERT INTO seg_usuario (nome, login, senha, contato, data_cadastro, status) VALUES (?, ?, ?, ?, NOW(), 1)");
            $stmt->bindValue(1, ($usuario_nome));
            $stmt->bindValue(2, $usuario_login);
            $stmt->bindValue(3, sha1($usuario_senha));
            $stmt->bindValue(4, $usuario_contato);

            $stmt->execute();

            $usuario_id = $db->lastInsertId();

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $usuario_id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O usuário foi cadastrado.';
            echo json_encode($msg);
            exit();
        }
    } else {
        $msg['msg'] = 'error';
        $msg['retorno'] = $mensagem;
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar salvar os dados do usuário: " . $e;
    echo json_encode($msg);
    exit();
}
?>

