
<?php

$db = Conexao::getInstance();

$id = (@$_POST['id']);
$codigo = isset($_POST['codigo']) && $_POST['codigo'] != "" ? (@$_POST['codigo']) : null;
$nome = isset($_POST['nome']) && $_POST['nome'] != "" ? (@$_POST['nome']) : null;
$qtd = isset($_POST['qtd']) && $_POST['qtd'] != "" ? (@$_POST['qtd']) : null;
$preco = isset($_POST['preco']) && $_POST['preco'] != "" ? (@$_POST['preco']) : null;
$fornecedor = isset($_POST['fornecedor']) && $_POST['fornecedor'] != "" ? (@$_POST['fornecedor']) : null;
$descricao = isset($_POST['descricao']) && $_POST['descricao'] != "" ? (@$_POST['descricao']) : null;

$error = false;

$mensagem = "";

$msg = array();

try {

    //VERIFICA SE O NOME DO PRODUTO JÁ FOI INFORMADO
    $id_nome = pesquisar("id", "produtos", "nome", "=", $nome, "");
    if (is_numeric($id_nome) && $id_nome != @$_POST['id']) {
        $error = true;
        $mensagem .= "<span>O nome do produto informado já existe no sistema.</span>";
        $msg['tipo'] = "nome";
    }

    //VERIFICA SE O CÓDIGO DO PRODUTO JÁ FOI INFORMADO
    $id_codigo = pesquisar("id", "produtos", "codigo", "=", $codigo, "");
    if (is_numeric($id_codigo) && $id_codigo != @$_POST['id']) {
        $error = true;
        $mensagem .= "<span>O código do produto informado já existe no sistema.</span>";
        $msg['tipo'] = "codigo";
    }

    if ($error == false) {

        $db->beginTransaction();

        if (isset($_POST['id']) && @$_POST['id'] != '') {//CASO O PRODUTO JÁ ESTEJA CADASTRADO
            $stmt = $db->prepare("UPDATE produtos set nome = ?, qtd = ?, valor = ?, fornecedor_id = ?, descricao = ?, codigo = ?, responsavel_id = ? WHERE id = ?");
            $stmt->bindValue(1, $nome);
            $stmt->bindValue(2, $qtd);
            $stmt->bindValue(3, $preco != null ? real2float($preco) : $preco);
            $stmt->bindValue(4, $fornecedor);
            $stmt->bindValue(5, $descricao);
            $stmt->bindValue(6, $codigo);
            $stmt->bindValue(7, $_SESSION['id']);
            $stmt->bindValue(8, $_POST['id']);

            $stmt->execute();

            $id = $_POST['id'];

            //ATUALIZANDO O NOME DA FOTO TEMPORARIA
            if (isset($_SESSION['foto_cut']) && isset($_SESSION['foto_origin']) && $_SESSION['foto_cut'] != "" && $_SESSION['foto_origin'] != "") {
                $stmt5 = $db->prepare("UPDATE produtos SET foto = ? WHERE id = ?");
                $stmt5->bindValue(1, $_SESSION['foto_cut']);
                $stmt5->bindValue(2, $id);
                $stmt5->execute();
            }

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O produto foi atualizado com sucesso!';
            echo json_encode($msg);
            exit();
        } else {//CASO O USUÁRIO AINDA NÃO ESTEJA CADASTRADO
            $stmt = $db->prepare("INSERT INTO produtos (nome, qtd, valor, fornecedor_id, descricao, codigo, data_cadastro, status, responsavel_id) VALUES ( ?, ?, ?, ?, ?, ?, NOW(), 1, ?)");
            $stmt->bindValue(1, $nome);
            $stmt->bindValue(2, $qtd);
            $stmt->bindValue(3, $preco != null ? real2float($preco) : $preco);
            $stmt->bindValue(4, $fornecedor);
            $stmt->bindValue(5, $descricao);
            $stmt->bindValue(6, $codigo);
            $stmt->bindValue(7, $_SESSION['id']);

            $stmt->execute();

            $id = $db->lastInsertId();

            //ATUALIZANDO O NOME DA FOTO TEMPORARIA
            if (isset($_SESSION['foto_cut']) && isset($_SESSION['foto_origin']) && $_SESSION['foto_cut'] != "" && $_SESSION['foto_origin'] != "") {
                $stmt5 = $db->prepare("UPDATE produtos SET foto = ? WHERE id = ?");
                $stmt5->bindValue(1, $_SESSION['foto_cut']);
                $stmt5->bindValue(2, $id);
                $stmt5->execute();
            }

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O produto foi cadastrado com sucesso!';
            echo json_encode($msg);
            exit();
        }
    } else {
        $msg['msg'] = 'error';
        $msg['retorno'] = $mensagem;
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar salvar os dados do produto: " . $e;
    echo json_encode($msg);
    exit();
}
?>

