
<?php

$db = Conexao::getInstance();

$id = @$_POST['id'];
$nome_fantasia = isset($_POST['nome_fantasia']) && $_POST['nome_fantasia'] != "" ? (@$_POST['nome_fantasia']) : null;
$razao_social = isset($_POST['razao_social']) && $_POST['razao_social'] != "" ? (@$_POST['razao_social']) : null;
$cpf = isset($_POST['cpf']) && $_POST['cpf'] != "" ? (@$_POST['cpf']) : null;
$cnpj = isset($_POST['cnpj']) && $_POST['cnpj'] != "" ? (@$_POST['cnpj']) : null;
$contato = isset($_POST['contato']) && $_POST['contato'] != "" ? (@$_POST['contato']) : null;
$endereco = isset($_POST['endereco']) && $_POST['endereco'] != "" ? (@$_POST['endereco']) : null;

$error = false;

$mensagem = "";

$msg = array();

try {

    //VERIFICA SE O NOME DO USUÁRIO JÁ FOI INFORMADO
    $id_nome = pesquisar("id", "fornecedores", "nome_fantasia", "=", $nome_fantasia, "");
    
    if (is_numeric($id_nome) && $id_nome != $id) {
        $error = true;
        $mensagem .= "<span>O nome do fornecedor informado já existe no sistema. $nome_fantasia </span>";
        $msg['tipo'] = "nome";
    }

    if ($error == false) {

        $db->beginTransaction();

        if (isset($_POST['id']) && @$_POST['id'] != '') {//CASO O USUÁRIO JÁ ESTEJA CADASTRADO
            $stmt = $db->prepare("UPDATE fornecedores SET nome_fantasia = ?, razao_social = ?, cpf = ?, cnpj = ?, contato = ?, endereco = ?, responsavel_id = ? WHERE id = ?");
            $stmt->bindValue(1, $nome_fantasia);
            $stmt->bindValue(2, $razao_social);
            $stmt->bindValue(3, $cpf);
            $stmt->bindValue(4, $cnpj);
            $stmt->bindValue(5, $contato);
            $stmt->bindValue(6, $endereco);
            $stmt->bindValue(7, $_SESSION['id']);
            $stmt->bindValue(8, $_POST['id']);

            $stmt->execute();

            $id = $_POST['id'];

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O fornecedor foi atualizado.';
            echo json_encode($msg);
            exit();
        } else {//CASO O USUÁRIO AINDA NÃO ESTEJA CADASTRADO
            $stmt = $db->prepare("INSERT INTO fornecedores (nome_fantasia, razao_social, cpf, cnpj, contato, endereco, data_cadastro, status, responsavel_id) VALUES (?, ?, ?, ?, ?, ?, NOW(), 1, ?)");
            $stmt->bindValue(1, $nome_fantasia);
            $stmt->bindValue(2, $razao_social);
            $stmt->bindValue(3, $cpf);
            $stmt->bindValue(4, $cnpj);
            $stmt->bindValue(5, $contato);
            $stmt->bindValue(6, $endereco);
            $stmt->bindValue(7, $_SESSION['id']);

            $stmt->execute();

            $id = $db->lastInsertId();

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $id;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'O fornecedor foi cadastrado.';
            echo json_encode($msg);
            exit();
        }
    } else {
        $msg['msg'] = 'error';
        $msg['retorno'] = $mensagem;
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar salvar os dados do fornecedor: " . $e;
    echo json_encode($msg);
    exit();
}
?>

