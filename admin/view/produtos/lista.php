<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<?php
if (isset($_POST['buscar_form'])) {
    $busca = ($_POST['buscar_form']);
} else {
    $busca = "";
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Produtos</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/painel">Início</a></li>
                <li>Lista</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0" style="color: black">Lista de Produtos</h3>
                <div class="dropdown dropleft text-right w-50 float-right">
                    <a class="btn btn-success" href="<?= PORTAL_URL; ?>admin/view/produtos/cadastro"> NOVO PRODUTO</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="scroll_horizontal_table" class="table dataTable-collapse dataTable no-footer" role="grid" aria-describedby="user_table_info" style="width: 100%">
                        <thead>
                            <tr>
                                <th>ITEM</th>
                                <th>NOME</th>
                                <th>QTD</th>
                                <th>FORNECEDOR</th>
                                <th>STATUS</th>
                                <th style="min-width: 100px;"></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $condicao = 1;
                            $cont = 1;
                            $utilizados = 0;
                            $disponiveis = 0;

                            if ($busca == "") {
                                $result = $db->prepare("SELECT * 
                                                     FROM produtos u  
                                                     WHERE 1 AND $condicao 
                                                     ORDER BY u.nome");
                            } else {
                                $result = $db->prepare("SELECT * 
                                                      FROM produtos u 
                                                      WHERE u.nome like ? AND $condicao 
                                                      ORDER BY u.nome");

                                $result->bindValue(1, "%$busca%");
                            }

                            $result->execute();

                            while ($material = $result->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <tr>
                                    <td><?= $material['codigo']; ?></td>
                                    <td><?= $material['nome']; ?></td>
                                    <td><?= $material['qtd']; ?></td>
                                    <td><?= pesquisar("nome_fantasia", "fornecedores", "id", "=", $material['fornecedor_id'], ""); ?> - <?= pesquisar("razao_social", "fornecedores", "id", "=", $material['fornecedor_id'], ""); ?></td>
                                    <td><span class="badge badge-pill <?= $material['status'] == 1 ? 'badge-success' : 'badge-danger'; ?> "><?= status($material['status']); ?></span></td>
                                    <td>
                                        <a href="<?= PORTAL_URL; ?>admin/view/produtos/cadastro/<?= $material['id']; ?>" title="Editar Produto" id='link_detalhar' class="text-info mr-2" rel="<?= $material['id']; ?>">
                                            <i class="i-Pen-4"></i>
                                        </a>

                                        <a title="Remover Produto" id="remover" rel="<?= $material['id']; ?>" class="text-danger mr-2"><i class="i-Close"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $cont++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php
include ('template/admin/rodape.php');
?>

<!-- JS DO MODULO-LISTA -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/produtos/lista.js"></script>

