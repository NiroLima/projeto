<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/plugins/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/plugins/cropper/css/main.css" rel="stylesheet">
<!-- FIM DO PLUGIN -->

<?php
$_SESSION['foto_cut'] = "";
$_SESSION['foto_origin'] = "";
unset($_SESSION['foto_cut']);
unset($_SESSION['foto_origin']);

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;
    $result = $db->prepare("SELECT *   
              FROM produtos
              WHERE id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_usuario = $result->fetch(PDO::FETCH_ASSOC);

    $produto_id = $dados_usuario['id'];
    $produto_codigo = $dados_usuario['codigo'];
    $produto_nome = $dados_usuario['nome'];
    $produto_qtd = $dados_usuario['qtd'];
    $produto_valor = $dados_usuario['valor'];
    $fornecedor_id = $dados_usuario['fornecedor_id'];
    $produto_descricao = $dados_usuario['descricao'];
    $produto_status = $dados_usuario['status'];
    $produto_foto = $dados_usuario['foto'];
} else {
    $produto_id = "";
    $produto_codigo = "";
    $produto_nome = "";
    $produto_qtd = "";
    $produto_valor = "";
    $fornecedor_id = "";
    $produto_descricao = "";
    $produto_status = 1;
    $produto_foto = "";
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Novo</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/painel">Início</a></li>
                <li><a href="<?= PORTAL_URL; ?>admin/view/produtos/lista">Lista</a></li>
                <li>Cadastro</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <div id="crop-avatar">
                        <!-- Current avatar -->
                        <div class="avatar-view" title="Trocar o Foto" style="float: left">
                            <?php
                            if ($produto_foto != "") {
                                ?>
                                <img src="<?= PORTAL_URL . "" . $produto_foto ?>" alt="Avatar"/>
                                <?php
                            } else {
                                ?>
                                <a href="#" class="photo_user"><img src="<?= PORTAL_URL ?>assets/img_produtos/sem_imagem.png" alt=""></a>
                                <?php
                            }
                            ?>
                        </div>

                        <!-- Cropping modal -->
                        <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal" type="button">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="avatar-body">

                                                <!-- Upload image and data -->
                                                <div class="avatar-upload">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                                </div>

                                                <!-- Crop and preview -->
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="avatar-wrapper"></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="avatar-preview preview-lg"></div>
                                                        <div class="avatar-preview preview-md"></div>
                                                        <div class="avatar-preview preview-sm"></div>
                                                    </div>
                                                </div>

                                                <div class="row avatar-btns">
                                                    <div class="col-md-9">
                                                        <div class="btn-group">
                                                            <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                                        </div>

                                                        <div class="btn-group" style="margin-top: 15px;">
                                                            <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                                            <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button class="btn btn-primary btn-block avatar-save" type="submit">Salvar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                        <!-- Loading state -->
                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                    </div>
                </fieldset>
            </div>
        </div>

        <br/>

        <form id="form_produto" name="form_produto" action="#" method="post">
            <input type="hidden" id="id" name="id" value="<?= $produto_id ?>"/>
            <div class="wrapper">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="w-50 float-left card-title m-0" style="color: black">Novo Produto</h3>
                    </div>
                    <div class="card-body pdl-2 pdr-2">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Infornações do Produto</legend>
                                    <div class="row">
                                        <div class="col-md-2 form-group mb-3">
                                            <label for="codigo">Código</label>
                                            <div id="div_codigo">
                                                <input type="text" id="codigo" name="codigo" placeholder="Código do Item" class="form-control" value="<?= $produto_codigo ?>"/>
                                            </div>
                                        </div>

                                        <div class="col-md-4 form-group mb-3">
                                            <label for="nome">Nome</label>
                                            <div id="div_nome">
                                                <input type="text" id="nome" name="nome" placeholder="Nome" class="form-control" value="<?= $produto_nome ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div id="div_qtds" class="col-md-3 form-group mb-3">
                                            <label for="qtd">Quantidade</label>
                                            <div id="div_qtd">
                                                <input type="text" onkeypress="return SomenteNumero(event);" id="qtd" name="qtd" placeholder="Quantidade" class="form-control" value="<?= $produto_qtd ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group mb-3">
                                            <label for="preco">Preço Unitário</label>
                                            <div id="div_preco">
                                                <input type="text" id="preco" name="preco" placeholder="Preço Unitário" class="form-control" value="<?= fdec($produto_valor) ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group mb-3">
                                            <label for="fornecedor">Fornecedores</label>
                                            <div id="div_fornecedor">
                                                <select id="fornecedor" name="fornecedor" class="form-control">
                                                    <option value="">Escolha o fornecedor</option>
                                                    <?php
                                                    $result = $db->prepare("SELECT * 
                                                        FROM fornecedores um  
                                                        WHERE um.status = 1 
                                                        ORDER BY um.nome_fantasia");
                                                    $result->execute();
                                                    while ($fornecedor = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($fornecedor_id == $fornecedor['id']) {
                                                            ?>
                                                            <option selected="true" value="<?= $fornecedor['id']; ?>"><?= $fornecedor['nome_fantasia'] != "" && $fornecedor['nome_fantasia'] != null ? $fornecedor['nome_fantasia'] : $fornecedor['razao_social']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?= $fornecedor['id']; ?>"><?= $fornecedor['nome_fantasia'] != "" && $fornecedor['nome_fantasia'] != null ? $fornecedor['nome_fantasia'] : $fornecedor['razao_social']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 form-group mb-3">
                                            <label for="descricao">Descrição</label>
                                            <div id="div_descricao">
                                                <textarea id="descricao" name="descricao" class="form-control"><?= $produto_descricao ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mgt-2">
                <div class="col-md-12 text-center">
                    <?php
                    if ($produto_id == "") {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Cadastrar</button>
                        <?php
                    } else {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Atualizar</button>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>

        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php include('template/admin/rodape.php'); ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL; ?>assets/plugins/cropper/js/cropper.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/plugins/cropper/js/main.js"></script>

<!-- JS DO USUARIO-CADASTRO -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/produtos/cadastro.js"></script>

