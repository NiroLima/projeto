<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<?php
if (isset($_POST['buscar_form'])) {
    $busca = ($_POST['buscar_form']);
} else {
    $busca = "";
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Fornecedores</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/painel">Início</a></li>
                <li>Lista</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0" style="color: black">Lista de Fornecedores</h3>
                <div class="dropdown dropleft text-right w-50 float-right">
                    <a class="btn btn-success" href="<?= PORTAL_URL; ?>admin/view/fornecedores/cadastro"> NOVO FORNECEDOR</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="scroll_horizontal_table" class="table dataTable-collapse dataTable no-footer" role="grid" aria-describedby="user_table_info" style="width: 100%">
                        <thead>
                            <tr>
                                <th style="min-width: 10px;">#</th>
                                <th>NOME FANTASIA</th>
                                <th>RAZÃO SOCIAL</th>
                                <th>CPF/CNPJ</th>
                                <th>STATUS</th>
                                <th style="min-width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $condicao = 1;
                            $cont = 1;
                            $utilizados = 0;
                            $disponiveis = 0;

                            if ($busca == "") {
                                $result = $db->prepare("SELECT * 
                                                     FROM fornecedores u  
                                                     WHERE 1 AND $condicao 
                                                     ORDER BY u.nome_fantasia");
                            } else {
                                $result = $db->prepare("SELECT * 
                                                      FROM fornecedores u 
                                                      WHERE u.nome_fantasia like ? AND $condicao 
                                                      ORDER BY u.nome_fantasia");

                                $result->bindValue(1, "%$busca%");
                            }

                            $result->execute();

                            while ($fornecedores = $result->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <tr>
                                    <td><?= $cont; ?></td>
                                    <td><?= $fornecedores['nome_fantasia']; ?></td>
                                    <td><?= $fornecedores['razao_social']; ?></td>
                                    <td><?= $fornecedores['cpf'] != "" && $fornecedores['cpf'] != null ? $fornecedores['cpf'] : $fornecedores['cnpj']; ?></td>
                                    <td><span class="badge badge-pill <?= $fornecedores['status'] == 1 ? 'badge-success' : 'badge-danger'; ?> "><?= status($fornecedores['status']); ?></span></td>
                                    <td>
                                        <a href="<?= PORTAL_URL; ?>admin/view/fornecedores/cadastro/<?= $fornecedores['id']; ?>" title="Editar Fornecedor" id='link_detalhar' class="text-info mr-2" rel="<?= $fornecedores['id']; ?>">
                                            <i class="i-Pen-4"></i>
                                        </a>

                                        <a title="Remover Fornecedor" id="remover" rel="<?= $fornecedores['id']; ?>" class="text-danger mr-2"><i class="i-Close"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $cont++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php
include ('template/admin/rodape.php');
?>

<!-- JS DO MODULO-LISTA -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/fornecedores/lista.js"></script>

