<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;
    $result = $db->prepare("SELECT *   
              FROM fornecedores
              WHERE id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_fornecedor = $result->fetch(PDO::FETCH_ASSOC);

    $fornecedor_id = $dados_fornecedor['id'];
    $nome_fantasia = $dados_fornecedor['nome_fantasia'];
    $razao_social = $dados_fornecedor['razao_social'];
    $cpf = $dados_fornecedor['cpf'];
    $cnpj = $dados_fornecedor['cnpj'];
    $contato = $dados_fornecedor['contato'];
    $endereco = $dados_fornecedor['endereco'];
    $status = $dados_fornecedor['status'];
} else {
    $fornecedor_id = "";
    $nome_fantasia = "";
    $razao_social = "";
    $cpf = "";
    $cnpj = "";
    $contato = "";
    $endereco = "";
    $status = 1;
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Novo</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/painel">Início</a></li>
                <li><a href="<?= PORTAL_URL; ?>admin/view/fornecedores/lista">Lista</a></li>
                <li>Cadastro</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <form id="form_fornecedor" name="form_fornecedor" action="#" method="post">
            <input type="hidden" id="id" name="id" value="<?= $fornecedor_id ?>"/>
            <div class="wrapper">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="w-50 float-left card-title m-0" style="color: black">Novo Fornecedor</h3>
                    </div>
                    <div class="card-body pdl-2 pdr-2">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Infornações do Fornecedor</legend>
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="nome_fantasia">Nome/Fantasia</label>
                                            <div id="div_nome_fantasia">
                                                <input type="text" id="nome_fantasia" name="nome_fantasia" placeholder="Nome Fantasia" class="form-control" value="<?= $nome_fantasia ?>"/>
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="razao_social">Razão Social</label>
                                            <div id="div_razao_social">
                                                <input type="text" id="razao_social" name="razao_social" placeholder="Razão Social" class="form-control" value="<?= $razao_social ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 form-group mb-3">
                                            <label for="cpf">CPF</label>
                                            <div id="div_cpf">
                                                <input type="text" data-mask="999.999.999-99" id="cpf" name="cpf" placeholder="CPF" class="form-control" value="<?= $cpf ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group mb-3">
                                            <label for="cnpj">CNPJ</label>
                                            <div id="div_cnpj">
                                                <input type="text" data-mask="99.999.999/9999-99" id="cnpj" name="cnpj" placeholder="CNPJ" class="form-control" value="<?= $cnpj ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group mb-3">
                                            <label for="contato">Contato</label>
                                            <div id="div_contato">
                                                <input type="text" data-mask="(99)9999-9999" id="contato" name="contato" placeholder="Contato" class="form-control" value="<?= $contato ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="endereco">Endereço</label>
                                            <div id="div_endereco">
                                                <input type="text" id="endereco" name="endereco" placeholder="Endereço" class="form-control" value="<?= $endereco ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mgt-2">
                <div class="col-md-12 text-center">
                    <?php
                    if ($fornecedor_id == "") {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Cadastrar</button>
                        <?php
                    } else {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Atualizar</button>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>

        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php include('template/admin/rodape.php'); ?>

<!-- JS DO USUARIO-CADASTRO -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/fornecedores/cadastro.js"></script>

