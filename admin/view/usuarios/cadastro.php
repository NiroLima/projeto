<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;
    $result = $db->prepare("SELECT *   
              FROM seg_usuario
              WHERE id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_usuario = $result->fetch(PDO::FETCH_ASSOC);

    $usuario_id = $dados_usuario['id'];
    $usuario_nome = $dados_usuario['nome'];
    $usuario_login = $dados_usuario['login'];
    $usuario_contato = $dados_usuario['contato'];
    $usuario_status = $dados_usuario['status'];
} else {
    $usuario_id = "";
    $usuario_nome = "";
    $usuario_login = "";
    $usuario_contato = "";
    $usuario_status = 1;
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <!-- ============ Body content start ============= -->
    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Novo</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/modulos">Início</a></li>
                <li><a href="<?= PORTAL_URL; ?>admin/view/usuarios/lista">Lista</a></li>
                <li>Cadastro</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <form id="form_usuario" name="form_usuario" action="#" method="post">
            <input type="hidden" id="id" name="id" value="<?= $usuario_id ?>"/>
            <div class="wrapper">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="w-50 float-left card-title m-0" style="color: black">Novo Usuário</h3>
                    </div>
                    <div class="card-body pdl-2 pdr-2">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Infornações Pessoais</legend>
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="nome">Nome</label>
                                            <div id="div_nome">
                                                <input type="text" id="nome" name="nome" placeholder="Nome" class="form-control" value="<?= $usuario_nome ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group mb-3">
                                            <label for="contato">Contato</label>
                                            <div id="div_contato">
                                                <input type="text" data-mask="(99)9999-9999" id="contato" name="contato" placeholder="Contato" class="form-control" value="<?= $usuario_contato ?>"/>
                                            </div>
                                        </div>
                                </fieldset>
                            </div>
                        </div>

                        <hr size="1" width="100%"/>

                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend>Informações de Acesso</legend>
                                    <div class="row">
                                        <div class="col-md-4 form-group mb-3">
                                            <label for="lgoin">Login</label>
                                            <div id="div_login">
                                                <input type="text" id="login" name="login" placeholder="Login" class="form-control" value="<?= $usuario_login ?>"/>
                                            </div>
                                        </div>

                                        <div class="col-md-4 form-group mb-3">
                                            <label for="senha">Senha</label>
                                            <div id="div_senha">
                                                <input type="password" id="senha" name="senha" placeholder="Senha" class="form-control" />
                                            </div>
                                        </div>

                                        <div class="col-md-4 form-group mb-3">
                                            <label for="senha2">Confirmar Senha</label>
                                            <div id="div_senha_2">
                                                <input type="password" id="senha2" name="senha2" placeholder="Confirmar Senha" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mgt-2">
                <div class="col-md-12 text-center">
                    <?php
                    if ($usuario_id == "") {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Cadastrar</button>
                        <?php
                    } else {
                        ?>
                        <button type="submit" class="btn btn-primary btn-lg"> Atualizar</button>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php include('template/admin/rodape.php'); ?>

<!-- JS DO USUARIO-CADASTRO -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/usuarios/cadastro.js"></script>

