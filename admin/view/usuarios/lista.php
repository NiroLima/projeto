<?php
include('template/admin/topo.php');
$db = Conexao::getInstance();
?>

<?php
if (isset($_POST['buscar_form'])) {
    $busca = ($_POST['buscar_form']);
} else {
    $busca = "";
}
?>

<div class="app-admin-wrap">
    <?php
    include ('template/admin/menu.php');
    ?>

    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="breadcrumb">
            <h1>Usuários</h1>
            <ul>
                <li><a href="<?= PORTAL_URL; ?>admin/painel">Início</a></li>
                <li>Lista</li>
            </ul>
        </div>

        <div class="separator-breadcrumb border-top"></div>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0" style="color: black">Lista de Usuários</h3>
                <div class="dropdown dropleft text-right w-50 float-right">
                    <a class="btn btn-success" href="<?= PORTAL_URL; ?>admin/view/usuarios/cadastro"> NOVO USUÁRIO</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="scroll_horizontal_table" class="table dataTable-collapse dataTable no-footer" role="grid" aria-describedby="user_table_info" style="width: 100%">
                        <thead>
                            <tr>
                                <th style="min-width: 10px;">#</th>
                                <th style="min-width: 300px;">NOME</th>
                                <th style="min-width: 100px;">LOGIN</th>
                                <th style="min-width: 100px;">CONTATO</th>
                                <th style="min-width: 70px;">STATUS</th>
                                <th style="min-width: 50px;"></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $condicao = 1;
                            $cont = 1;

                            if ($busca == "") {
                                $result = $db->prepare("SELECT * 
                                                     FROM seg_usuario u  
                                                     WHERE 1 AND $condicao 
                                                     ORDER BY u.nome");
                            } else {
                                $result = $db->prepare("SELECT * 
                                                      FROM seg_usuario u 
                                                      WHERE u.nome like ? AND $condicao 
                                                      ORDER BY u.nome");

                                $result->bindValue(1, "%$busca%");
                            }

                            $result->execute();

                            while ($usuario = $result->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <tr>
                                    <td><?= $cont; ?></td>
                                    <td><?= $usuario['nome']; ?></td>
                                    <td><?= $usuario['login']; ?></td>
                                    <td><?= str_replace(" ", "", ($usuario['contato'])); ?></td>
                                    <td><span class="badge badge-pill <?= $usuario['status'] == 1 ? 'badge-success' : 'badge-danger'; ?> "><?= status($usuario['status']); ?></span></td>
                                    <td>
                                        <a <?= $usuario['id'] == 1 ? "style='display: none'" : ""; ?> href="<?= PORTAL_URL; ?>admin/view/usuarios/cadastro/<?= $usuario['id']; ?>" title="Editar Servidor" id='link_detalhar' class="text-info mr-2" rel="<?= $usuario['id']; ?>">
                                            <i class="i-Pen-4"></i>
                                        </a>

                                        <a title="Remover Usuário" id="remover" rel="<?= $usuario['id']; ?>" class="text-danger mr-2"><i class="i-Close"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $cont++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        include ('template/admin/footer.php');
        ?>
    </div>
</div>

<?php
include ('template/admin/rodape.php');
?>

<!-- JS DO MODULO-LISTA -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/usuarios/lista.js"></script>

