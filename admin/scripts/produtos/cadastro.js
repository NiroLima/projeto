//------------------------------------------------------------------------------
$(document).ready(function () {
    $("input#preco").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    $("input#litro").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("select#fornecedor").select2();

//------------------------------------------------------------------------------
//SALVANDO DADOS DO FORMULÁRIO
    $('#form_produto').submit(function () {
        if (formulario_validator("")) {
            window.onbeforeunload = null;
            projetouniversal.util.getjson({
                url: PORTAL_URL + "admin/dao/produtos/cadastro",
                type: "POST",
                data: $('#form_produto').serialize(),
                enctype: 'multipart/form-data',
                success: onSuccessSend,
                error: onError
            });
            return false;
        } else {
            return false;
        }
    });
//------------------------------------------------------------------------------
    function onSuccessSend(obj) {
        if (obj.msg == 'success') {
            swal({
                title: "Sucesso!",
                text: "" + obj.retorno + "",
                type: "success",
                confirmButtonClass: "btn btn-success",
                confirmButtonText: "Ok"
            }).then(function () {
                postToURL(PORTAL_URL + 'admin/view/produtos/lista');
            });
        } else if (obj.msg == 'error') {
            formulario_validator(obj);
        }
        return false;
    }
//------------------------------------------------------------------------------
    /* ERRO AO ENVIAR AJAX */
    function onError(args) {
        $.prompt('onError: ' + args.retorno);
    }
});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var nome = $("#nome").val();
    var codigo = $("#codigo").val();
    var qtd = $("#qtd").val();
    var preco = $("#preco").val();
    var fornecedor = $("#fornecedor").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO DATA DE NASCIMENTO FOI INFORMADO
        if (fornecedor == "") {
            $('div#div_fornecedor').after('<label id="erro_fornecedor" class="error">Fornecedor é obrigatório.</label>');
            valido = false;
            element = $('div#div_fornecedor');
        }

        //VERIFICANDO SE O CAMPO DATA DE NASCIMENTO FOI INFORMADO
        if (preco == "" || preco == "0,00") {
            $('div#div_preco').after('<label id="erro_preco" class="error">Preço é obrigatório.</label>');
            valido = false;
            element = $('div#div_preco');
        }

        //VERIFICANDO SE O CAMPO DATA DE NASCIMENTO FOI INFORMADO
        if (qtd == "") {
            $('div#div_qtd').after('<label id="erro_qtd" class="error">Quantidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_qtd');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome == "") {
            $('div#div_nome').after('<label id="erro_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (codigo == "") {
            $('div#div_codigo').after('<label id="erro_codigo" class="error">Código é obrigatório.</label>');
            valido = false;
            element = $('div#div_codigo');
        }

    } else if (obj.tipo == "nome") {
        $('div#div_nome').after('<label id="erro_nome" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_nome');
    } else if (obj.tipo == "codigo") {
        $('div#div_codigo').after('<label id="erro_codigo" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_codigo');
    } else if (obj.tipo == "qtd") {
        $('div#div_qtd').after('<label id="erro_qtd" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_qtd');
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function SomenteNumero(e) {

    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return false;
    }
}
//------------------------------------------------------------------------------