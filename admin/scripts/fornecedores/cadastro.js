//------------------------------------------------------------------------------
$(document).ready(function () {
    $("input#preco").priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
//------------------------------------------------------------------------------
//SALVANDO DADOS DO FORMULÁRIO DO USUÁRIO
    $('#form_fornecedor').submit(function () {
        if (formulario_validator("")) {
            window.onbeforeunload = null;
            projetouniversal.util.getjson({
                url: PORTAL_URL + "admin/dao/fornecedores/cadastro",
                type: "POST",
                data: $('#form_fornecedor').serialize(),
                enctype: 'multipart/form-data',
                success: onSuccessSend,
                error: onError
            });
            return false;
        } else {
            return false;
        }
    });
//------------------------------------------------------------------------------
    function onSuccessSend(obj) {
        if (obj.msg == 'success') {
            swal({
                title: "Sucesso!",
                text: "" + obj.retorno + "",
                type: "success",
                confirmButtonClass: "btn btn-success",
                confirmButtonText: "Ok"
            }).then(function () {
                postToURL(PORTAL_URL + 'admin/view/fornecedores/lista');
            });
        } else if (obj.msg == 'error') {
            formulario_validator(obj);
        }
        return false;
    }
    //------------------------------------------------------------------------------
    /* ERRO AO ENVIAR AJAX */
    function onError(args) {
        $.prompt('onError: ' + args.retorno);
    }
});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var nome_fantasia = $("#nome_fantasia").val();
    var cpf = $("#cpf").val();
    var cnpj = $("#cnpj").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO DATA DE NASCIMENTO FOI INFORMADO
        if (cpf == "" && cnpj == "") {
            $('div#div_cpf').after('<label id="erro_cpf" class="error">É necessário informar o CPF ou CNPJ.</label>');
            $('div#div_cnpj').after('<label id="erro_cpf" class="error">É necessário informar o CPF ou CNPJ.</label>');
            valido = false;
            element = $('div#div_cpf');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_fantasia == "") {
            $('div#div_nome_fantasia').after('<label id="erro_nome_fantasia" class="error">O nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_fantasia');
        }

    } else if (obj.tipo == "nome") {
        $('div#div_nome_fantasia').after('<label id="erro_nome_fantasia" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_nome_fantasia');
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------