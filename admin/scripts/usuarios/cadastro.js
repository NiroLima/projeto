//------------------------------------------------------------------------------
$(document).ready(function () {
//------------------------------------------------------------------------------
//SALVANDO DADOS DO FORMULÁRIO DO USUÁRIO
    $('#form_usuario').submit(function () {
        if (formulario_validator("")) {
            window.onbeforeunload = null;
            projetouniversal.util.getjson({
                url: PORTAL_URL + "admin/dao/usuarios/cadastro",
                type: "POST",
                data: $('#form_usuario').serialize(),
                enctype: 'multipart/form-data',
                success: onSuccessSend,
                error: onError
            });
            return false;
        } else {
            return false;
        }
    });
//------------------------------------------------------------------------------
    function onSuccessSend(obj) {
        if (obj.msg == 'success') {
            swal({
                title: "Sucesso!",
                text: "" + obj.retorno + "",
                type: "success",
                confirmButtonClass: "btn btn-success",
                confirmButtonText: "Ok"
            }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/usuarios/lista');
            });
        } else if (obj.msg == 'error') {
            formulario_validator(obj);
        }
        return false;
    }
    //------------------------------------------------------------------------------
    /* ERRO AO ENVIAR AJAX */
    function onError(args) {
        $.prompt('onError: ' + args.retorno);
    }
});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var id = $("#id").val();
    var nome = $("#nome").val();
    var login = $("#login").val();
    var senha = $("#senha").val();
    var senha2 = $("#senha2").val();
    var contato = $("#contato").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS
        //VERIFICANDO SE O CAMPO LOGIN FOI INFORMADO
        if (senha == "" && id == "") {
            $('div#div_senha').after('<label id="erro_senha" class="error">A senha é obrigatório.</label>');
            valido = false;
            element = $('div#div_senha');
        } else {
            //VERIFICANDO SE O CAMPO LOGIN FOI INFORMADO
            if (senha != senha2) {
                valido = false;

                $('div#div_senha').after('<label id="erro_senha" class="error">A senha e confirmação de senha não coincidem.</label>');
                $('div#div_senha_2').after('<label id="erro_senha_2" class="error">A senha e confirmação de senha não coincidem.</label>');

                element = $('div#div_senha_2');
            }
        }

        //VERIFICANDO SE O CAMPO LOGIN FOI INFORMADO
        if (login == "") {
            $('div#div_login').after('<label id="erro_login" class="error">Login é obrigatório.</label>');
            valido = false;
            element = $('div#div_login');
        }

        //VERIFICANDO SE O CAMPO DATA DE NASCIMENTO FOI INFORMADO
        if (contato == "") {
            $('div#div_contato').after('<label id="erro_contato" class="error">Contato é obrigatório.</label>');
            valido = false;
            element = $('div#div_contato');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome == "") {
            $('div#div_nome').after('<label id="erro_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome');
        }

    } else if (obj.tipo == "cpf") {//VALIDAÇÃO COM BANCO DE DADOS
        $('div#div_cpf').after('<label id="erro_cpf" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_cpf');
    } else if (obj.tipo == "login") {
        $('div#div_login').after('<label id="erro_login" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_login');
    } else if (obj.tipo == "nome") {
        $('div#div_nome').after('<label id="erro_nome" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_nome');
    } else if (obj.tipo == "atualizada") {
        Messenger().post({
            message: obj.retorno + '<br/><br/><center><button id="atualizar_pagina" class="btn btn-primary">Recarregar</button></center>',
            type: 'success',
            showCloseButton: true
        });
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------