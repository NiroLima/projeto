<?php
@session_start();

include_once('conf/config.php');

// VERIFICAÇÕES DE SESSÕES
if (isset($_SESSION['id'])) {
    echo "<script>window.location = '" . PORTAL_URL . "admin/painel';</script>";
    exit();
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
        <meta name="application-name" content="&nbsp;"/>
        <meta name="theme-color" content="#000000"/>
        <title>E-COMMERCE::LOGIN</title>

        <!-- Icons -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= PORTAL_URL; ?>assets/img/9072logotelles.ico">

        <!-- Plugins -->
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/plugins/bootstrap/css/bootstrap.min.css"/>
        <!-- Font Face -->
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/fonts/fonts.css"/>
        <!-- Font Icons -->
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/fontawesome/css/all.css"/>
        <!-- Style -->
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/login.css"/>

    </head>
    <body>

        <div class="limiter">
            <div class="container-login100" style="background-image: url('<?= PORTAL_URL; ?>assets/img/acre/01.jpg');">
                <div class="fade-login gradient-blue"></div>
                <div class="wrap-login100 p-t-30 p-b-50">
                    <form id="form_login" name="form_login" method="post" action="#" class="login100-form validate-form p-b-33 p-t-5">

                        <div id="div_login" class="wrap-input100 validate-input" data-validate="Informe seu usuário">
                            <input id="login" name="login" class="input100" type="text" placeholder="Usuário">
                      
                        </div>

                        <div id="div_senha" class="wrap-input100 validate-input" data-validate="Informe sua senha">
                            <input id="senha" name="senha" class="input100" type="password" placeholder="Senha">
                      
                        </div>

                        <div class="container-login100-form-btn m-t-32">
                            <button class="login100-form-btn">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

<!-- jQuery  -->
<script src="<?= PORTAL_URL; ?>assets/js/jquery-1.9.1.min.js"></script>

<!-- Operações Js -->
<script type="text/javascript" src="<?= PORTAL_URL ?>conf/config.js"></script>

<!-- JS DO LOGIN -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/login.js"></script>