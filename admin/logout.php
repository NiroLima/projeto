<?php
@session_start();
include_once('conf/config.php');
include_once('conf/Url.php');

if (isset($_SESSION['id'])) {
    $sessao = $_SESSION['id'];
} else {
    $sessao = 0;
}

$idsessao = session_id();

session_unset();
session_destroy();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>E-COMMERCE</title>
        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="your,keywords">
        <meta name="description" content="Short explanation about this website">
    </head>
    <body>
        <?php
        unset($_SESSION['id']);
        echo "<script 'text/javascript'>window.location = '" . PORTAL_URL . "admin/login';</script>";
        ?>
    </body>
</html>