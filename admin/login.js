//------------------------------------------------------------------------------
$(document).ready(function () {
    $('#form_login').submit(function () {
        if (login_validator()) {
            $.ajax({
                type: "POST",
                url: PORTAL_URL + 'admin/autenticar',
                data: $('#form_login').serialize(),
                cache: false,
                success: function (obj) {
                    obj = JSON.parse(obj);
                    if (obj.msg == 'success') {
                        setTimeout("location.href='" + PORTAL_URL + "admin/painel'", 1);
                    } else if (obj.msg == 'error') {
                        $('div#div_senha').after('<center><label id="erro_senha" style="color: red">' + obj.retorno + '</label></center>');
                    }
                },
                error: function (obj) {
                    alert(obj.retorno);
                }
            });
            return false;
        } else {
            return false;
        }
    });
});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function login_validator() {
    var valido = true;
    var login = $("#login").val();
    var senha = $("#senha").val();

    //LIMPA MENSAGENS DE ERRO
    $('label#erro_login').remove();
    $('label#erro_senha').remove();

    //VERIFICANDO SE OS CAMPOS LOGIN E SENHA FORAM INFORMADOS
    if (login == "") {
        $('div#div_login').after('<label id="erro_login" class="error" style="color: red; text-align: center; width: 100%;">O campo usuário é obrigatório.</label>');
        valido = false;
    }
    if (senha == "") {
        $('div#div_senha').after('<label id="erro_senha" class="error" style="color: red; text-align: center; width: 100%;">O campo senha é obrigatório.</label>');
        valido = false;
    }
    return valido;
}
//------------------------------------------------------------------------------