<?php

ob_start();
session_start();

//ADICIONAR A CONEXAO E URL AMIGAVEL
include_once("conf/Url.php");
include_once("conf/config.php");
include_once("operations/funcoes.php");
include_once("conf/session.php");

//INSTANCIA A CONEXAO
$db = Conexao::getInstance();

$modulo = Url::getURL(0);
$mvc = Url::getURL(1);
$arquivomodulo = Url::getURL(2);
$parametromodulo = Url::getURL(3);


//VERIFICA SE O ARQUIVO EXISTE E EXIBI
if (file_exists($modulo . ".php") && $modulo != 'portal') {
    include_once $modulo . ".php";
    sessionOn();
    exit();
}

if ($modulo == 'index.php' || $modulo == 'index' || $modulo == '' || $modulo == null) {
    $modulo = "portal";
    include_once $modulo . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'login.php' || $mvc == 'login') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'painel.php' || $mvc == 'painel') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'logout.php' || $mvc == 'logout') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'autenticar.php' || $mvc == 'autenticar') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else {

    if ($parametromodulo == 'index.php' || $parametromodulo == 'index' || $parametromodulo == '' || $parametromodulo == null) {

        //VERIFICA SE O ARQUIVO EXISTE E EXIBI
        if (file_exists($modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . "index.php")) {
            include_once $modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . "index.php";
            sessionOn();
            exit();
        } else {
            include_once "404.php";
            sessionOn();
            exit();
        }
    } else {
        if ($arquivomodulo == '' || $arquivomodulo == null) {

            //VERIFICA SE O ARQUIVO EXISTE E EXIBI
            if (file_exists($modulo . '/' . $mvc . '/' . "index.php")) {
                include_once $modulo . '/' . $mvc . '/' . "index.php";
                sessionOn();
                exit();
            } else {
                include_once "404.php";
                sessionOn();
                exit();
            }
        } else {
            //VERIFICA SE O ARQUIVO EXISTE E EXIBI
            if (file_exists($modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . $parametromodulo . ".php")) {
                include_once $modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . $parametromodulo . ".php";
                sessionOn();
                exit();
            } else {
                include_once "404.php";
                sessionOn();
                exit();
            }
        }
    }//END IF
}//END IF
?>  