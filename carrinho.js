$(document).ready(function () {
    $(".camera_prev").hide();
    $(".camera_next").hide();
});
//------------------------------------------------------------------------------
$('body').mouseover(function () {
    $("div#car_search").slideUp("slow");
});
//------------------------------------------------------------------------------
$('div#div_modal_car').mouseover(function () {
    event.stopPropagation();
});
//------------------------------------------------------------------------------
$("ul#icone_carrinho").mouseover(function () {
    $("div#car_search").slideDown("slow");
    event.stopPropagation();
});
//------------------------------------------------------------------------------
function atualiza(obj, codigo) {

    var qtd = $(obj).val();

    window.onbeforeunload = null;
    projetouniversal.util.getjson({
        url: PORTAL_URL + "atualiza_carrinho",
        type: "POST",
        data: {id: codigo, qtd: qtd},
        enctype: 'multipart/form-data',
        success: onSuccessSend2,
        error: onError
    });
    return false;

}
//------------------------------------------------------------------------------
function comprar() {
    swal({
        title: "Antenção!",
        text: "É necessário realizar seu cadastro e login no sistema para continuar!",
        type: "warning",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    });
    return false;
}

//------------------------------------------------------------------------------
function deslizar(op) {
    if (op == 0) {
        var topPosition = $("body").offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    } else if (op == 1) {
        var topPosition = $("#menu_produto").offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
}
//------------------------------------------------------------------------------
function mais(codigo) {
    window.onbeforeunload = null;
    projetouniversal.util.getjson({
        url: PORTAL_URL + "salvar_carrinho",
        type: "POST",
        data: {id: codigo},
        enctype: 'multipart/form-data',
        success: onSuccessSend2,
        error: onError
    });
    return false;
}
//------------------------------------------------------------------------------
function menos(codigo) {
    window.onbeforeunload = null;
    projetouniversal.util.getjson({
        url: PORTAL_URL + "menos",
        type: "POST",
        data: {id: codigo},
        enctype: 'multipart/form-data',
        success: onSuccessSend2,
        error: onError
    });
    return false;
}
//------------------------------------------------------------------------------
function remover(codigo) {
    swal({
        title: "Deseja mesmo remover este produto?",
        text: "Obs: Caso escolha remover, seu produto vai sair do seu carrinho!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Cancelar"
    }).then(function () {
        window.onbeforeunload = null;
        projetouniversal.util.getjson({
            url: PORTAL_URL + "remover_carrinho",
            type: "POST",
            data: {id: codigo},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });

}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {
    if (obj.msg == 'success') {
        swal({
            title: "Sucesso!",
            text: "" + obj.retorno + "",
            type: "success",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "Ok"
        }).then(function () {
            postToURL(PORTAL_URL + 'carrinho');
        });
    } else if (obj.msg == 'error') {
        alert('Erro: ' + obj.retorno);
    }
    return false;
}
//------------------------------------------------------------------------------
function onSuccessSend2(obj) {
    if (obj.msg == 'success') {
        postToURL(PORTAL_URL + 'carrinho');
    } else if (obj.msg == 'error') {
        alert('Erro: ' + obj.retorno);
    }
    return false;
}
//------------------------------------------------------------------------------
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    alert('Erro: ' + args.retorno);
}

//------------------------------------------------------------------------------
