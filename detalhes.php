<?php include_once('template/header_car.php'); ?>

<br/><br/><br/>

<div id="content-wrapper-parent">
    <div id="content-wrapper">  
        <!-- Content -->
        <div id="content" class="clearfix">        
            <section class="content">
                <div id="col-main" class="clearfix">
                    <div class="home-popular-collections">

                        <br/>

                        <hr size="1" style="width: 100%" />

                        <br/><br/>

                        <?php
                        $id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
                        $param = Url::getURL(1);
                        $param = $param == '' && $id != '' ? $id : $param;

                        $qtd = 0;

                        $busca = $param;

                        if ($busca != "" && is_numeric($busca)) {
                            $result = $db->prepare("SELECT p.id, p.descricao, p.valor, p.nome, p.foto 
                                                      FROM produtos p 
                                                      WHERE p.id = ? 
                                                      ORDER BY p.nome");
                            $result->bindValue(1, $busca);
                            $result->execute();
                            $qtd = $result->rowCount();
                        } else if (isset($_POST['resultado'])) {

                            $busca = $_POST['resultado'];

                            $result = $db->prepare("SELECT p.id, p.descricao, p.valor, p.nome, p.foto 
                                                      FROM produtos p 
                                                      WHERE p.nome LIKE ? 
                                                      ORDER BY p.nome");
                            $result->bindValue(1, "%" . $busca . "%");
                            $result->execute();
                            $qtd = $result->rowCount();
                        } else {
                            $result = $db->prepare("SELECT p.id, p.descricao, p.valor, p.nome, p.foto 
                                                      FROM produtos p 
                                                      WHERE p.status = 1 
                                                      ORDER BY p.nome");
                            $result->execute();
                            $qtd = $result->rowCount();
                        }

                        if ($qtd > 0) {
                            while ($produto = $result->fetch(PDO::FETCH_ASSOC)) {
                                ?>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-6 text-center">
                                        <img src="<?= PORTAL_URL . "" . ($produto['foto'] != "" ? $produto['foto'] : "assets/img_produtos/sem_imagem.png") ?>" style="width: 300px; height: 300px;"/>
                                    </div>

                                    <div class="col-md-10 prod-descricao">
                                        <h2 class="h2-nome-prod"><?= $produto['nome'] ?></h2>
                                        <?= $produto['descricao'] ?>
                                    </div>

                                    <div class="col-md-6 prod-valores">
                                        <p><h2 class="detalhe-valor">R$ <?= fdec($produto['valor']); ?></h2></p>
                                        <p>em até 12x sem juros no <b>cartão de crédito do E-commerce</b> e receba <b class="green-color">R$ 10,00 de volta</b></p>
                                        <p>R$ <?= fdec(desconto($produto['valor'], 4)); ?> <b class="green-color">(4% de desconto)</b> em até 15x sem juros no cartão do E-commerce e receba <b class="green-color">R$ 10,00 de volta</b></p>

                                        <br/>

                                        <div class="text-center"><button onclick="comprar(<?= $produto['id'] ?>)" class="btn btn-primary" style="width: 80%">Add ao Carrinho</button></div>

                                    </div>
                                    <div class="col-md-1"></div>
                                </div>

                                <br/><br/>

                                <hr size="1" style="width: 100%" />

                                <br/><br/>

                                <?php
                            }
                        } else {
                            echo "<div style='width: 100%; text-align: center;'>Nenhum produto encontrado!</div>";
                        }
                        ?>
                    </div>
                </div>
            </section>        
        </div>
    </div>
</div>

<?php include_once('template/footer.php'); ?>

<!-- JS DA PÁGINA PORTAL -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>detalhes.js"></script>