<?php include_once('template/header_car.php'); ?>

<br/><br/><br/>

<div id="content-wrapper-parent">
    <div id="content-wrapper">  
        <!-- Content -->
        <div id="content" class="clearfix">        
            <section class="content">
                <div id="col-main" class="clearfix">
                    <div class="home-popular-collections">

                        <br/>

                        <hr size="1" style="width: 100%" />

                        <div class="container text-center">
                            <div class="group_home_collections row">
                                <div class="col-md-24">
                                    <div class="home_collections">
                                        <div class="home_collections_wrapper">
                                            <h1 style="color: black">Meu Carrinho</h1>
                                            <?php
                                            $qtd = 0;
                                            $busca = array();

                                            $allItems = $cart->getItems();

                                            foreach ($allItems as $items) {
                                                foreach ($items as $item) {
                                                    $qtd++;
                                                    array_push($busca, $item['id']);
                                                }
                                            }

                                            if ($qtd > 0) {
                                                $result = $db->prepare("SELECT p.id, p.descricao, p.valor, p.nome, p.foto  
                                                      FROM produtos p 
                                                      WHERE p.id IN (" . implode(",", $busca) . ") 
                                                      ORDER BY p.nome");
                                                $result->execute();
                                            }

                                            if ($qtd > 0) {
                                                ?>
                                                <table id="scroll_horizontal_table" class="table dataTable-collapse dataTable no-footer" role="grid" aria-describedby="user_table_info" style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 200px;"></th>
                                                            <th>PRODUTO</th>
                                                            <th>DESCRIÇÃO</th>
                                                            <th style="width: 100px;">QTD.</th>
                                                            <th style="width: 150px;">PREÇO</th>
                                                            <th style="width: 150px;">TOTAL</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $total_itens = 0;
                                                        $valor_total = 0;

                                                        while ($produto = $result->fetch(PDO::FETCH_ASSOC)) {

                                                            $quantidade = $cart->lookItem($produto['id'], "quantity");

                                                            $total_itens += $quantidade;
                                                            $valor_total += $quantidade * $produto['valor'];
                                                            ?>
                                                            <tr>
                                                                <td class="text-center-important"><img src="<?= PORTAL_URL . "" . ($produto['foto'] != "" ? $produto['foto'] : "assets/img_produtos/sem_imagem.png") ?>" class="carrinho-foto"/></td>
                                                                <td class="text-left"><?= $produto['nome'] ?></td>
                                                                <td class="text-left"><?= $produto['descricao'] ?></td>
                                                                <td>
                                                                    <div class="mais-menos">
                                                                        <a style="cursor: pointer" onclick="menos(<?= $produto['id']; ?>)"><i class="fa fa-minus"></i></a>&nbsp;&nbsp;&nbsp;<input type="text" onblur="atualiza(this, <?= $produto['id']; ?>)" id="qtd" name="qtd" value="<?= $quantidade; ?>" class="form-control"/>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer" onclick="mais(<?= $produto['id']; ?>)"><i class="fa fa-plus"></i></a>
                                                                    </div>
                                                                </td>
                                                                <td>R$ <?= fdec($produto['valor']); ?></td>
                                                                <td>R$ <?= fdec($quantidade * $produto['valor']); ?></td>
                                                                <td class="text-center-important"><a title="Remover Produto" id="remover" onclick="remover(<?= $produto['id']; ?>)" class="text-danger mr-2 remover-prod"><i class="fa fa-trash icone-lixeira-20"></i></a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <?php
                                            } else {
                                                echo "<div style='width: 100%; text-align: center;'>Nenhum produto encontrado!</div>";
                                            }
                                            ?>
                                        </div>

                                        <div class="row produto-total">
                                            <div class="col-md-24 text-right">
                                                <div class="col-md-18">
                                                    <b class="total-size-22">Total</b> <a class="resultado-itens">(<?= $total_itens; ?> <?= $total_itens > 1 ? "itens" : "item"; ?>)</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <b>R$ <?= fdec($valor_total); ?> </b>ou<br/>
                                                    <b>R$ <?= fdec(desconto($valor_total, 4)); ?></b> <b class="green-color">(4% de desconto)</b> à vista
                                                    <p></p>
                                                    <p><b>R$ <?= fdec(somar_desconto($valor_total, 30)); ?></b> em até <b class="green-color">12x no cartão</b> de crédito do <b class="green-color">E-commerce</b></p>
                                                    <div class="text-right"><button onclick="comprar()" class="btn btn-primary" style="width: 80%">Comprar</button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>        
        </div>
    </div>
</div>

<?php include_once('template/footer.php'); ?>

<!-- JS DA PÁGINA PORTAL -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>carrinho.js"></script>